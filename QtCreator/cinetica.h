#ifndef CINETICA_H
#define CINETICA_H

#include <QDialog>
#include <dialog.h>
#include <cinetica.h>
#include <QMainWindow>
#include <QRectF>
#include <QRect>
#include <string>
#include <QtCharts>
#include <QChart>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QLegend>
#include <QBarCategoryAxis>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QWidget>
#include <QtGlobal>
namespace Ui {
class cinetica;
}

class cinetica : public QDialog
{
    Q_OBJECT

public:
    explicit cinetica(QWidget *parent = nullptr);
    ~cinetica();

private slots:
    void on_CalcularFisica_clicked();

private:
    Ui::cinetica *ui;
};

#endif // CINETICA_H
