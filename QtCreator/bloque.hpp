#ifndef BLOQUE
#define BLOQUE
#include <dialog.h>
#include <cinetica.h>
#include <QMainWindow>
#include <QRectF>
#include <QRect>
#include <string>
#include <QtCharts>
#include <QChart>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QLegend>
#include <QBarCategoryAxis>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QWidget>
#include <QtGlobal>



namespace Ui {
class bloque;
}

class bloque : public QMainWindow
{
    Q_OBJECT

public:
    explicit bloque(QWidget *parent = 0);
    ~bloque();

private slots:


    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

protected:
    Ui::bloque *ui;



};


#endif // BLOQUE_H
