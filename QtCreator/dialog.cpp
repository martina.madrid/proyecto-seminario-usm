#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

float *reau = NULL;
float *rank = NULL;
float *fah=NULL;
float *kel=NULL;
float *cel = NULL;
float cel1;
float kel1;
float fah1;
float reau1;
float rank1;
void Dialog::on_calcularMate_clicked()
{
    QString usuario2 = ui->dato2->text();

    float celcius = usuario2.toFloat();

    float kelvin = +273 + celcius;

    float faren = celcius * 1.8 + 32;

    float reaumur = celcius/1.25;

    float rankine = celcius*1.8 +491.67;


    ui->lcdnumber_2->display(celcius);
    ui->lcdnumber_3->display(kelvin);
    ui->lcdnumber_4->display(faren);
    ui->lcdnumber_5->display(reaumur);
    ui->lcdnumber_6->display(rankine);

    cel=new float;
    *cel=celcius;
    cel1=*cel;

    kel=new float;
    *kel=kelvin;
    kel1=*kel;

    fah=new float;
    *fah=faren;
    fah1=*fah;

    reau=new float;
    *reau=reaumur;
    reau1=*reau;

    rank=new float;
    *rank=rankine;
    rank1=*rank;


    QBarSet *set0 = new QBarSet("CELSIUS");
    QBarSet *set1 = new QBarSet("KELVIN");
    QBarSet *set2 = new QBarSet("FAHRENHEIT");
    QBarSet *set3 = new QBarSet("REAUMUR");
    //QBarSet *set4 = new QBarSet("RANKINE");
    *set0 << cel1;
    *set1 << kel1;
    *set2 << fah1;
    *set3 << reau1;
    //*set4 << rank1;

    // Crear series
    QBarSeries *series = new QBarSeries;
    series->append(set0);
    series->append(set1);
    series->append(set2);
    series->append(set3);
    //series->append(set4);

    series->setLabelsVisible(true);
    // Crear gráfico e insertar serie
        QChart *chart = new QChart();
        chart->setTitle("TEMPERATURAS");
        chart->legend()->setVisible(true);
        chart->legend()->setAlignment(Qt::AlignBottom);
        chart->addSeries(series);
        chart->setAnimationOptions(QChart::SeriesAnimations);
        chart->createDefaultAxes();

        //Crear widget QChartView (e insertarlo en una ventana):
        ui->graphicsView->setChart(chart);
        ui->graphicsView->show();
}

