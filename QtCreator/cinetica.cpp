#include "cinetica.h"
#include "ui_cinetica.h"

float ener1;
float *ener=NULL;

cinetica::cinetica(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cinetica)
{
    ui->setupUi(this);
}

cinetica::~cinetica()
{
    delete ui;
}

void cinetica::on_CalcularFisica_clicked()
{



        QString usuario = ui->dato2->text();

        float masa = usuario.toFloat();

        QString usuario3 = ui->dato2_2->text();

        float Vincial = usuario3.toFloat();


        float energiaCinetica_inicial;
        energiaCinetica_inicial = ( (0.5) * masa ) * (Vincial * Vincial);

        ui->lcdnumber_2->display(energiaCinetica_inicial);


        //-----------------------------------------------//

            ener=new float;
            *ener=energiaCinetica_inicial;
            ener1=*ener;



            QBarSet *set0 = new QBarSet("Energia cinetica inicial");

            *set0 << ener1;
            // Crear series
            QBarSeries *series = new QBarSeries;
            series->append(set0);

            series->setLabelsVisible(true);
            // Crear gráfico e insertar serie
                QChart *chart = new QChart();
                chart->setTitle("Energia cinetica inicial");
                chart->legend()->setVisible(true);
                chart->legend()->setAlignment(Qt::AlignBottom);
                chart->addSeries(series);
                chart->setAnimationOptions(QChart::SeriesAnimations);
                chart->createDefaultAxes();

                //Crear widget QChartView (e insertarlo en una ventana):
                ui->graphicsView->setChart(chart);
                ui->graphicsView->show();

}

