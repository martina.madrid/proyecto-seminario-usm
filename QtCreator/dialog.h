#ifndef DIALOG_H
#define DIALOG_H
#include <dialog.h>
#include <cinetica.h>
#include <QMainWindow>
#include <QRectF>
#include <QRect>
#include <string>
#include <QtCharts>
#include <QChart>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QLegend>
#include <QBarCategoryAxis>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QWidget>
#include <QtGlobal>
#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_calcularMate_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
