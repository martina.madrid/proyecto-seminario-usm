#include <iostream>
#include <cstdlib>
#include <cmath>
#include <math.h>

using namespace std;

//MODULO MATEMÁTICO

void dimension(){
    int opcionDimension;
    float kilometros, hectarias, metro, decimetros, centimetros, 
milimetros, micrometros, nanometros, milla, yarda, pie, pulgada, onzas, galones;
        
    cout << "\t\tTRANSFORMADOR DE DIMENSIONES:"<<endl;
    cout << "\nIngrese la dimensión que desea transformar:"<<endl;
    cout << "\t1. Área"<<endl;
    cout << "\t2. Longitud"<<endl;
    cout << "\t3. Volumen"<<endl;
    cin >> opcionDimension;
        
    switch (opcionDimension){
        case 1:
        float area;

        cout << "\t\tDIMENSIÓN: ÁREA"<<endl;
        cout << "\nIngrese la unidad que desea transformar en metros cuadrados [m²]: "<<endl;
        cin >> area;

        kilometros = area * (1 * pow(10, -6));
        cout << "Unidad en Kilometros al cuadrado = " << kilometros << " [Km²]" << endl;
        hectarias = area * (1 * pow (10, -4));
        cout << "Unidad en Hectaría al cuadrado = " << hectarias << " [ha²]" << endl;
        decimetros = area * (1 * pow(10, 3));
        cout << "Unidad en Decimetros al cuadrado = " << decimetros << "[dm²]" << endl;
        centimetros = area * (1 * pow(10, 4));
        cout << "Unidad en Centímetros al cuadrado = " << centimetros << " [cm²]" << endl;
        milimetros = area * (1 * (pow(10, 6)));
        cout << "Unidad en Milimetros al cuadrado = " << milimetros << " [mm²]" << endl;
        micrometros = area * (1 * (pow(10, 12)));
        cout << "Unidad en Micrometros al cuadrado = " << micrometros << " [µm²]" << endl;
        nanometros = area * (1 * (pow(10, 18)));
        cout << "Unidad en Nanometros al cuadrado = " << nanometros << " [nm²]" << endl;
        milla = area * (3.86 * pow(10,-7));
        cout << "Unidad en Milla al cuadrado = " << milla << " [mi²]" << endl;
        yarda = area * 1.196;
        cout << "Unidad en Yarda al cuadrado = " << yarda << " [yr²]" << endl;
        pie = area * 10.76;
        cout << "Unidad en Pie al cuadrado = " << pie << " [ft²]" << endl;
        pulgada = area * 1550;
        cout << "Unidad en Pulgada al cuadrado = " << pulgada << " [in²]" << endl;
    
    break;

        case 2:
        float longitud;
        cout << "\t\tDIMENSION: LONGITUD" << endl;
        cout << "\nIngrese la unidad que desea transformar en metros [m]: " << endl;
        cin >> longitud;
                
        kilometros = longitud * (1 *(pow(10, -3)));
        cout<< "Unidad en Kilometros = "<< kilometros << " [Km]" << endl;
        hectarias = longitud * (1 *(pow(10, -4)));
        cout<<"Unidad en Hectarías = " << hectarias << " [ha]" << endl;
        decimetros = longitud * 10;
        cout<<"Unidad en Decimetros = " << decimetros << " [dm]" << endl;
        centimetros= longitud * 100;
        cout<<"Unidad en Centímetros = "<< centimetros << " [cm]" << endl;
        milimetros = longitud * 1000;
        cout<<"Unidad en Milimetros = "<< milimetros << " [mm]" << endl;
        micrometros = longitud * 1000000;
        cout<<"Unidad en Micrometros = "<< micrometros << "[µm]" << endl;
        nanometros = longitud * (1 *(pow(10, 9)));
        cout<<"Unidad en Nanometros = "<< micrometros << "[nm]" << endl;
        milla = longitud * 0.0006;
        cout<<"Unidad en Milla = "<< milla << " [mi]" << endl;
        yarda = longitud * 1.094;
        cout<<"Unidad en Yarda = "<< yarda << " [yr]" << endl;
        pie = longitud * 3.28;
        cout<<"Unidad en Pie = "<< pie << " [ft]" << endl;
        pulgada = longitud * 39.37;
        cout<<"Unidad en Pulgada = "<< pulgada << " [in]" << endl;
    
    break;

        case 3:
        float volumen;
        cout<< "\t\tDIMENSIONES: VOLÚMEN"<<endl;
        cout<< "\nIngrese la unidad que desea transformar en metros cúbicos [m³]: "<<endl;
        cin>>volumen;

        kilometros = volumen * (1* pow(10,-9));
        cout << "Unidad en Kilometros cúbicos = " << kilometros << " [Km³]" << endl;
        onzas = volumen * 33814.02;
        cout << "Unidad en Onzas = "<< onzas << " [Oz]" << endl;
        decimetros = volumen * 1000;
        cout << "Unidad en Decimetros cúbicos = " << decimetros << " [dm³]" << endl;
        centimetros = volumen * 1000000;
        cout << "Unidad en Centimetros cúbicos = " << centimetros << " [cm³]" << endl;
        milimetros = volumen * 1000000000;
        cout << "Unidad en Milimetros cúbicos = " << milimetros << "[mm³]" << endl;
        yarda = volumen * 1.308;
        cout << "Unidad en Yarda cúbica = " << yarda << "[yr³]" << endl;
        pie = volumen * 35.31;
        cout << "Unidad en Pie cúbicos = " << pie << "[ft³]" << endl;
        pulgada = volumen * 61023;
        cout << "Unidad en Pulgada cúbicos =  " << pulgada << " [in³]" << endl;
        galones = volumen * 227.02;
        cout << "Unidad en Galones cúbicos =  " << galones << "[gal³]" << endl;

    break;

        default: 
            cout<< "La opción no es correcta."<<endl;

    break;

    }
}

void movimiento(){
    int opcionMovimiento;
    cout << "\t\tTRANSFORMADOR DE MOVIMIENTO" << endl;
    cout << "\nIngrese la opción de movimiento que desea transformar: " << endl;
    cout << "\t1. Velocidad" << endl;
    cout << "\t2. Aceleración" << endl;
    cin >> opcionMovimiento;
    
    switch(opcionMovimiento){
        case 1:
        float velocidad;
        float m_s, km_h, mm_s, micro_s, milla_s, milla_h, pie_s, velocidad_luz, velocidad_sonido;
        cout << "\t\tMOVIMIENTO: VELOCIDAD" << endl;
        cout << "\nIngrese la unidad que desea transformar en kilometros por segundo [Km/s]: " << endl;
        cin >> velocidad;

        m_s = velocidad * 1000;
        cout << "Unidad en Metro por segundo = " << m_s << "[m/s]" << endl;
        km_h = velocidad * 3600;
        cout << "Unidad en Kilometro por hora = " << km_h << "[Km/h]" << endl;
        mm_s = velocidad * 1000000;
        cout << "Unidad en milimetro por segundo = " << mm_s << "[mm/s]" << endl;
        micro_s = velocidad * 1000000000;
        cout << "Unidad en Micrometro por segundo = " << micro_s << "[µm/s]" << endl;
        milla_s = velocidad * 0.00017;
        cout << "Unidad en Milla por segundo = "<< milla_s << "[mi/s]" << endl;
        milla_h = velocidad * 0,621371;
        cout << "Unidad en Milla por hora = " <<  milla_h << "[mi/h]" << endl;
        pie_s = velocidad * 0,911344;
        cout << "Unidad en Pies por segundo = " << pie_s << "[ft/s]" << endl;
        velocidad_luz = velocidad * (9.27 * pow(10, -10));
        cout << "Unidad en Velocidad de la luz = "<< velocidad_luz << "[Km/s]" << endl;
        velocidad_sonido = velocidad * 0.000809848;
        cout << "Unidad en Velocidad del sonido = " << velocidad_sonido << "[m/s]" << endl;

    break;

        case 2:

        float aceleracion;
        float km_h2, km_m2, micro_s2, milla_s2, milla_h2, pie_s2, gals;
        cout << "\t\tMOVIMIENTO: ACELERACIÓN" << endl;
        cout << "\nIngrese la unidad que desea transformar en metros por segundo al cuadrado [m/s²]: " << endl;
        cin >> aceleracion;

        km_h2 = aceleracion * 12960;
        cout << "Unidad en Kilometro por hora = " << km_h2 << " [Km/h]" << endl;
        km_m2 = aceleracion * 3.6;
        cout << "Unidad en Kilometro por minuto = " << km_m2 << " [Km/min]" << endl;
        milla_h2 = aceleracion * 8053;
        cout << "Unidad en milla por hora = " << milla_h2 << " [mi/h]" << endl;
        pie_s2 = aceleracion * 3.2808;
        cout << "Unidad en Pie por segundo = " << pie_s2 << "[ft/s]" << endl;
        gals = aceleracion * 100;
        cout << "Unidad en Gal = " << gals << "[Gal]" << endl;

    break;

    default: 
        cout<< "La opción no es correcta."<<endl;

    break;
    }
}

void electricidad(){
    int opcionElectricidad;
    cout << "\t\tTRANSFORMADOR DE ELECTRICIDAD" << endl;
    cout << "\nIngrese la opción que desea: " << endl;
    cout << "\t1. Carga eléctrica." << endl;
    cout << "\t2. Intensidad elétrica." << endl;
    cout << "\t3. Potencial eléctrico." << endl;
    cout << "\t4. Resistencia eléctrica." << endl;
    cin >> opcionElectricidad;

    switch(opcionElectricidad){
    case 1:
        float culombios;
        float nano_c, micro_c, mili_c, kilo_c, mega_c, mili_a, amperio_h, faraday, elemental;
        cout << "\t\tELECTRICIDAD: CARGA ELÉCTRICA" << endl;
        cout << "\nIngrese la carga que desea transformar en Culombios [C]: " << endl;
        cin >> culombios;

        nano_c = culombios * 1000000000;
        cout << "Unidad en NanoCulombios = " << nano_c << " [nC]" << endl;
        micro_c = culombios * 1000000;
        cout << "Unidad en microCulombios = " << nano_c << " [µC]" << endl;
        mili_c = culombios * 1000;
        cout << "Unidad en miliCulombios = " << nano_c << " [mC]" << endl;
        kilo_c = culombios * pow(10,-3);
        cout << "Unidad en kiloCulombios = " << nano_c << " [kC]" << endl;
        mega_c = culombios * pow(10,-6);
        cout << "Unidad en megaCulombios = " << nano_c << " [MC]" << endl;
        mili_a = culombios * 0.28;
        cout << "Unidad en miliAmperios = " << nano_c << " [mA]" << endl;
        amperio_h = culombios * (2.78*pow(10,-4));
        cout << "Unidad en Amperios = " << nano_c << " [A]" << endl;
        faraday = culombios * (1.04*pow(10,-5));
        cout << "Unidad en Faradio = " << nano_c << " [F]" << endl;
        elemental = culombios * (6,24*pow(10,18));
        cout << "Unidad en Carga elemental= " << nano_c << " [e⁻]" << endl;

    break;

    case 2:
        float amperios;
        float nano_a, micro_a, mili_a2, kilo_a, mega_a, giga_a, ab_a;
        cout << "\t\tELECTRIICIDAD: INTENSIDAD ELECTRICA"<<endl;
        cout << "\nIngrese la carga que desea transformar en Amperios [A]: "<<endl;
        cin >> amperios;

        nano_a = amperios * 1000000000;
        cout << "Unidad en Nanoamperios = " << nano_a << " [nA]" << endl;
        micro_a = amperios * 1000000;
        cout << "Unidad en Microamperios = " << micro_a << " [µA]" << endl;
        mili_a2 = amperios * 1000;
        cout << "Unidad en Miliamperios = " << mili_a2 << " [mA]" << endl;
        kilo_a = amperios * pow(10,-3);
        cout << "Unidad en Kiloamperios = " << kilo_a << " [kA]" << endl;
        mega_a = amperios * pow(10,-6);
        cout << "Unidad en Megamperios =  " << mega_a << " [MA]" << endl;
        giga_a = amperios * pow(10,-9);
        cout << "Unidad en Gigamperios = " << giga_a << " [GA]" << endl;
        ab_a = amperios * 0.1;
        cout << "Unidad en Abamperios = " << ab_a << " [abΩ]" <<endl;
    
    break;

    case 3:
        float potencialElectrico;
        float nano_v, micro_v, mili_v, kilo_v, mega_v, giga_v, ab_v;
        cout << "\t\tELECTRICIDAD: POTENCIAL ELECTRICO" << endl;
        cout << "\nIngrese la carga que desea transformar en Voltios [V]: " << endl;
        cin >> potencialElectrico;

        nano_v = potencialElectrico * 1000000000;
        cout << "Unidad en Nanovoltios = " << nano_v << "[nV]" << endl;
        micro_v = potencialElectrico * 1000000;
        cout << "Unidad en Microvoltios = " << micro_v << "[µV]" << endl;
        mili_v = potencialElectrico * 1000;
        cout << "Unidad en Milivoltios = " << mili_v << "[mV]" << endl;
        kilo_v = potencialElectrico * pow(10,-3);
        cout << "Unidad en Kilovoltios = " << kilo_v << "[kV]" << endl;
        mega_v = potencialElectrico * pow(10,-6);
        cout << "Unidad en Megavoltios = " << mega_v << "[MV]" << endl;
        giga_v = potencialElectrico * pow(10,-9);
        cout << "Unidad en Gigavoltios = " << giga_v << "[GV]" << endl;
        ab_v = potencialElectrico * 100000000;
        cout << "Unidad en Abvoltios = " << ab_v << "[abV]"<< endl;
    
    break;

    case 4:
        float ohm;
        float nano_o, micro_o, mili_o, kilo_o, mega_o, giga_o, ab_o;
        cout << "\t\tELECTRICIDAD: RESISTENCIA ELECTRICA" << endl;
        cout << "\nIngrese la opción que desea en Ohmios: " << endl;
        cin >> ohm;

        nano_o = ohm* 1000000000;
        cout << "Unidad en Nanoohmios = " << nano_o << "[nΩ]" << endl;
        micro_o = ohm* 1000000;
        cout << "Unidad en Microohmios = " << nano_o << "[µΩ]" << endl;
        mili_o = ohm* 1000;
        cout << "Unidad en Miliohmios = " << nano_o << "[mΩ]" << endl;
        kilo_o = ohm* pow(10,-3);
        cout << "Unidad en Kiloohmios = " << nano_o << "[kΩ]" << endl;
        mega_o = ohm* pow(10,-6);
        cout << "Unidad en Megaohmios = " << nano_o << "[MΩ]" << endl;
        giga_o = ohm* pow(10,-9);
        cout << "Unidad en Gigaohmios = " << nano_o << "[GΩ]" << endl;
        ab_o = ohm* 1000000000;
        cout << "Unidad en Abohmios = " << nano_o << "[abΩ]" << endl;

    break;
            
    default: 
        cout<< "La opción no es correcta."<<endl;

    break;
    }
}
    
void bytes(){

    float bytes;
    float byte_kilobyte, byte_megabyte, byte_gigabyte, byte_terabyte, byte_petabyte, byte_exabyte;
    cout << "\t\tALMACENAMIENTO DE DATOS" << endl;
    cout << "\nIngrese la opción que desea transformar en bytes [Bytes]: " << endl;
    cin >> bytes;

    byte_kilobyte = bytes * (9.77 * pow(10, -4));
    cout << "Unidad en KiloBytes = " << byte_kilobyte << "[kBytes]" << endl;
    byte_megabyte = bytes * (9.54 * pow(10, -7));
    cout << "Unidad en MegaBytes = " << byte_megabyte<< "[NBytes]" << endl;
    byte_gigabyte = bytes * (9.31 * pow(10, -10));
    cout << "Unidad en GigaBytes = " << byte_gigabyte << "[GBytes]" << endl;
    byte_terabyte = bytes * (9.09 * pow(10, -13));
    cout << "Unidad en TeraBytes = " << byte_terabyte << "[TBytes]" << endl;
    byte_petabyte = bytes * (8.88 * pow(10, -16));
    cout << "Unidad en PetaBytes = " << byte_petabyte << "[PBytes]" << endl;
    byte_exabyte = bytes * (8.67 * pow(10, -19));
    cout << "Unidad en ExaBytes = " << byte_exabyte << "[EBytes]" << endl;
}

void energia(){
int opcion1, opcion2;
float megajulio, kilojulio, julio, millivatio, vatio, kilovatio, julio_psegundo, celsius, kelvin, fahrenheit, reaumur, rankine;

    cout << "\t\tTRANSFORMADOR DE ENERGÍA" << endl;    
    cout << "1. MAGNITUD DE ENERGIA" << endl;
    cout << "2. MAGNITUD DE POTENCIA" << endl; 
    cout << "3. CONVERSION DE GRADOS (TEMPERATURA)" << endl;
    cin >> opcion1;

    switch (opcion1){
    
        case 1:
    
        cout << "\t\tMAGNITUD DE ENERGIA [Kilovatio]---" << endl;
        cout << "\t1. Kilovatio a Megajulio" << endl;
        cout << "\t2. KIlovatio a Kilojulio" << endl;
        cout << "\t3. Kilovatio a Julio" << endl;
        cout << "\nIngrese la opcion que desea: " << endl;
        cin >> opcion2;

    switch (opcion2){

        case 1:
        cout << "\t\tKilovatio a Megajulio--" << endl;
        cout << "\nIngrese la cantidad de Kilovatios a convertir [kV]: " << endl;
        cin >> kilovatio;
        megajulio = kilovatio * ((3,77) * pow(10,-7));
        cout << kilovatio << "Kilovatios equivalen a = " << megajulio << " [MJ]" << endl;
    break;

        case 2:
        cout << "\t\tKilovatio a Kilojulio" << endl;
        cout << "\nIngrese la cantidad de Kilovatios a convertir: " << endl;
        cin >> kilovatio;
        kilojulio = kilovatio * ((1,36) * (10,-6));
        cout << kilovatio << "Kilovaltios equivalen a = " << kilojulio << " [kJ]" << endl;
        break;

        case 3:
        cout << "\t\tKilovatio a Julio---" << endl;
        cout << "\nIngrese la cantidad de Kilovatios a convertir: " << endl;
        cin >> kilovatio;
        julio = kilovatio * 1,36;
        cout << kilovatio << "Kilovaltios equivalen a = " << julio << " [J]" << endl;
        break;

    }
    
    break; 

    case 2:

        cout << "\t\tMAGNITUD DE POTENCIA [Millivatio]" << endl;
        cout << "\nIngrese la opcion que desea: " << endl;
        cout << "\t1. Millivatio a Vatio" << endl;
        cout << "\t2. Millivatio a Kilovatio" << endl;
        cout << "\t3. Millivatio a Julio por Segundo" << endl;
        cin >> opcion2;

    switch (opcion2){

        case 1:
            cout << "\t\tMillivatio a Vatio" << endl;
            cout << "\nIngrese la cantidad de Millivatios a convertir: " << endl;
            cin >> millivatio;
            vatio = millivatio * pow(10,-3);
            cout << millivatio << "Millivatios equivalen a = " << vatio << " [W]" << endl;
            break;

        case 2:
            cout << "\t\tMillivatio a Kilovatio---" << endl;
            cout << "\nIngrese la cantidad de Millivatios a convertir: " << endl;
            cin >> millivatio;
            kilovatio = millivatio * pow(10,-6);
            cout << millivatio << "Millivatios equivalen a = " << kilovatio << " [kW]" << endl;
            break;

        case 3:
            cout << "\t\tMillivatio a Julio por Segundo" << endl;
            cout << "\nIngrese la cantidad de Millivatios a convertir: " << endl;
            cin >> millivatio;
            julio_psegundo = millivatio * pow(10,-3);
            cout << millivatio << "Millivatios equivalen a = " << julio_psegundo << " [J/s]" << endl;
            break;

    }
    break; 

    case 3:

    cout << "\t\tCONVERSION DE TEMPERATURA [CELSIUS]" << endl;
    cout << "\nIngrese la opcion que desea: " << endl;
    cout << "\t1. Celsius a Kelvin" << endl;
    cout << "\t2. Celsius a Fahrenheit" << endl;
    cout << "\t3. Celsius a Reaumur" << endl;
    cout << "\t4. Celsius a Rankine" << endl;
    cin >> opcion2;

    switch (opcion2){

        case 1:
            cout << "\t\tCelsius a Kelvin" << endl;
            cout << "\nIngrese la cantidad de grados a convertir: " << endl;
            cin >> celsius;
            kelvin = celsius * 255,93; 
            cout << celsius << "Grados Celsius equivalen a = " << kelvin << " grados Kelvin." << endl;
            break;

        case 2:
            cout << "\t\tCelsius a Fahrenheit---" << endl;
            cout << "\nIngrese la cantidad de grados a convertir: " << endl;
            cin >> celsius;
            fahrenheit = celsius * 1;
            cout << celsius << "Grados Celsius equivalen a = " << fahrenheit << " grados Fahrenheit." << endl;
            break;

        case 3:
            cout << "---Celsius a Reaumur---" << endl;
            cout << "Ingrese la cantidad de grados a convertir: : " << endl;
            cin >> celsius;
            reaumur = celsius * (-13,78);
            cout << celsius << "Grados Celsius equivalen a " << reaumur << " grados reaumur." << endl;
            break;

        case 4:
            cout << "\t\tCelsius a Rankine" << endl;
            cout << "\nIngrese la cantidad de grados a convertir: " << endl;
            cin >> celsius;
            rankine = celsius * (460,67);
            cout << celsius << "Grados Celsius equivalen a = " << rankine << " grados Rankine." << endl;
            break;


        default:
            cout << "La opcion ingresada no se encuentra en el menu." << endl;
        break;
        }
    }
}

void quimica(){
    int opcion1, opcion2;
float gramo_centimetro, gramo_metro, gramo_mol, kilogramo_metro, kilogramo_mol;

cout << "INGRESE LA OPCION QUE DESEA: " << endl;
cout << "1. MEDIDAS DE DENSIDAD" << endl;
cout << "2. MEDIDAS DE MASA MOLAR" << endl; 
cin >> opcion1;

switch (opcion1){
    
    case 1:
    
    cout << "---MEDIDAS DE DENSIDAD---" << endl;
    cout << "Ingrese la opcion que desea: " << endl;
    cout << "1. Gramo por centìmetro cúbico a Kilogramo por metro cúbico" << endl;
    cout << "2. Gramo por centìmetro cúbico a Kilogramo a Gramo por metro cúbico" << endl;
    cout << "3. Kilogramo por metro cubico a Gramo por centimetro cubico" << endl;
    cout << "4. Kilogramo por metro cubico a Gramo por metro cubico" << endl; 
    cin >> opcion2;

    switch (opcion2){

        case 1:
            cout << "---Gramo por centìmetro cúbico a Kilogramo por metro cúbico---" << endl;
            cout << "Ingrese la cantidad de gramos por centimetros cubicos a convertir: " << endl;
            cin >> gramo_centimetro;
            kilogramo_metro = gramo_centimetro * pow(10,-3);
            cout << gramo_centimetro << "Gramos por centimetros cubicos equivalen a " << kilogramo_metro << " kilogramos por metros cubicos." << endl;
            break;

        case 2:
            cout << "---Gramo por centìmetro cúbico a Kilogramo a Gramo por metro cúbico---" << endl;
            cout << "Ingrese la cantidad de gramos por centimetros cubicos a convertir: " << endl;
            cin >> gramo_centimetro;
            gramo_metro = gramo_centimetro * pow(10,-3); 
            cout << gramo_centimetro << "Gramos por centimetros cubicos equivalen a " << gramo_metro << " gramos por metros cubicos." << endl;
            break;

        case 3:
            cout << "---Kilogramo por metro cubico a Gramo por centimetro cubico---" << endl;
            cout << "Ingrese la cantidad de kilogramos por metro cubico a convertir: " << endl;
            cin >> kilogramo_metro;
            gramo_centimetro = kilogramo_metro / 1;
            cout << kilogramo_metro << "Kilogramos por metros cubicos equivalen a " << gramo_centimetro << " gramos por centimetros cubicos." << endl;
            break;

        case 4:
            cout << "---Kilogramo por metro cubico a Gramo por metro cubico---" << endl;
            cout << "Ingrese la cantidad de kilogramos por metro cubico a convertir: " << endl;
            cin >> kilogramo_metro;
            gramo_metro = kilogramo_metro / 1;
            cout << kilogramo_metro << "Kilogramos por metros cubicos equivalen a " << gramo_metro << " gramos por metros cubicos." << endl;
            break;

    }
    
    break; 

case 2:

    cout << "---MEDIDAS DE MASA MOLAR---" << endl;
    cout << "Ingrese la opcion que desea: " << endl;
    cout << "1. Gramos por mol a Kilogramos por mol" << endl;
    cout << "2. Kilogramos por mol a Gramos por mol" << endl;
    cin >> opcion2;

    switch (opcion2){

        case 1:
            cout << "---Gramos por mol a Kilogramos por mol---" << endl;
            cout << "Ingrese la cantidad de Gramos por moles a convertir: " << endl;
            cin >> gramo_mol;
            kilogramo_mol = gramo_mol * (1,01);
            cout << gramo_mol << "Gramos por mol equivalen a " << gramo_mol << " kilogramos por mol." << endl;
            break;

        case 2: 
            cout << "---Kilogramos por mol a Gramos por mol---" << endl;
            cout << "Ingrese la cantidad de Kilogramos por moles a convertir: " << endl;
            cin >> kilogramo_mol;
            gramo_mol = kilogramo_mol / ((1,01) * pow(10,-3));
            cout << kilogramo_mol << "Kilogramos por mol equivalen a " << kilogramo_mol << " gramos por mol." << endl;
            break;
    }


default:

    cout << "La opcion ingresada no se encuentra en el menu." << endl;
    break;
}

}

void tiempo(){
    double tiempo;
    double segundos_nanosegundos, segundos_microsegundos, segundos_milisegundos,  segundos_dias;
    cout << "\t\tTIEMPO" << endl;
    cout << "\nIngrese la unidad que desea transformar en segundos [seg]: " << endl;
    cin >> tiempo;

    segundos_nanosegundos = tiempo * pow(10, 9);
    cout << "Unidad a nanoSegundos = " << segundos_nanosegundos << " [nSeg]" << endl;
    segundos_microsegundos = tiempo * pow(10, 6);
    cout << "Unidad a microSegundos = " << segundos_microsegundos << " [nSeg]" << endl;
    segundos_milisegundos = tiempo * pow(10, 3);
    cout << "Unidad a miliSegundos = " << segundos_milisegundos << " [mSeg]" << endl;
    double segundos_minutos = tiempo * 0.016666667;
    cout << "Unidad a minutos = " << segundos_minutos << " [min]" << endl;
    double segundos_horas = tiempo * 0.0003125;
    cout << "Unidad a horas = " << segundos_horas << " [hrs]" << endl;
    segundos_dias = tiempo * 0.000011574;
    cout << "Unidad a días = " << segundos_dias << " [días]" << endl;
}

//MODULO FÍSICO

void masa(){
    float kilogramo, gramo, libra, miligramo, onza, tonelada;
    cout <<  "\t\tMASA" << endl;
    cout << "\nIngrese la unidad que desea transformar en Kilogramos [Kg]: " << endl;
    cin >> kilogramo; 

    gramo = kilogramo * 1000;
    cout << "Unidad en Gramos [gr] = " << gramo << "[gr]" << endl;
    miligramo = kilogramo * 1000000; 
    cout << "Unidad en miligramos [mg] = " << gramo << "[mg]" << endl;
    libra = kilogramo * 2.20462;
    cout << "Unidad en libras [lb] = " << gramo << "[lb]" << endl;
    onza = kilogramo * 35.2;
    cout << "Unidad en Onzas [Oz] = " << gramo << "[Oz]" << endl;
    tonelada = kilogramo * 0.001;
    cout << "Unidad en toneladas [Ton] = " << gramo << "[Ton]" << endl;

}
float energiaCinetica_inicial1;

float energiaCinetica_inicial_1(float masa, float velocidad_inicial){
    float energiaCinetica_inicial;
    energiaCinetica_inicial = ( (0.5) * masa ) * (velocidad_inicial * velocidad_inicial);
    energiaCinetica_inicial1 = energiaCinetica_inicial;
    return energiaCinetica_inicial;
}

float energiaCinetica_final1;

float energiaCinetica_final_1(float masa, float velocidad_final){
    float energiaCinetica_final;
    energiaCinetica_final = ( (0.5) * masa ) * (velocidad_final * velocidad_final);
    energiaCinetica_final1 = energiaCinetica_final;
    return energiaCinetica_final;
}


float diferencialCineticas_1(float energiaInicial, float energiaFinal){
    float diferencialCinetica;
    diferencialCinetica = energiaFinal - energiaInicial;
    return diferencialCinetica;  
}

void moduloCinetico(){

    float masa;
    float velocidad_inicial;
    float velocidad_final;

    cout << "MÓDULO FÍSICO:\t\tCALCULADORA DE ENERGÍA CINÉTICA" << endl;
    cout << "\nPor favor, a continuación ingrese los datos solicitados:" << endl;

    cout << "\nMasa del objeto puntual en [Kg]:" << endl;
    cin >> masa;

    while (masa <= 0)
    {
        cout<<"Debe ser un valor mayor a cero. Reintente..." <<endl;
        cin >> masa ;
    }

    cout << "\nVelocidad incial del objeto puntual en [m/s²]: " << endl;
    cin >> velocidad_inicial;

    while (velocidad_inicial == 0)
    {
        cout<<"Debe ser un valor distinto a cero. Reintente..." <<endl;
        cin >> velocidad_inicial ;
    }

    cout << "\nLa Energía Cinética inicial del sistema corresponde a = " << energiaCinetica_inicial_1(masa, velocidad_inicial)<< " [Julios]." << endl;
    cout << "\nVelocidad final del objeto puntual en [m/s²]: " << endl;
    cin >> velocidad_final;

    cout << "\nLa Energía Cinética final del sistema corresponde a = " << energiaCinetica_final_1(masa, velocidad_final) << " [Julios]." << endl;
    cout <<"\nAhora, el diferencial de energías corrrespode a " << diferencialCineticas_1(energiaCinetica_inicial1, energiaCinetica_final1) << " [Julios]. " << endl;

    if (energiaCinetica_inicial_1(masa, velocidad_inicial) > energiaCinetica_final_1(masa, velocidad_final)){

        cout << "\nSu objeto se encuentra desacelerando." << endl;

    } else if (energiaCinetica_inicial_1(masa, velocidad_inicial) < energiaCinetica_final_1(masa, velocidad_final)) {

        cout << "\nSu objeto se encuentra acelerando." << endl;

    } else {

        cout <<"\nSu objeto se encuentra a una velocidad constante." << endl;
    }
}

float potencialInicial_1;

float energiaPotencial_inicial_1(float masa, float altura_inicial){

    float energiaPotencial_inicial;
    energiaPotencial_inicial = masa * 9.8 * altura_inicial;
    potencialInicial_1= energiaPotencial_inicial;
    return energiaPotencial_inicial;
}

float potencialFinal_1;

float energiaPotencial_final_1(float masa, float altura_final){

    float energiaPotencial_final;
    energiaPotencial_final = masa * 9.8 * altura_final;
    potencialFinal_1 = energiaPotencial_final;
    return energiaPotencial_final;
}


float diferencialPotencial_1(float potencialFinal_1, float potencialInicial_1){

    float diferencialPotencial;
    diferencialPotencial = (potencialFinal_1 - potencialInicial_1);
    return diferencialPotencial;  
}

void moduloPotencial(){

    float masa;
    float altura_inicial;
    float altura_final;

    cout << "MÓDULO FÍSICO:\t\tCALCULADORA DE ENERGÍA POTENCIAL" << endl;
    cout << "\nPor favor, a continuación ingrese los datos solicitados:" << endl;

    cout << "\nMasa del objeto puntual en [Kg]:" << endl;
    cin >> masa;

    while (masa <= 0)
    {
        cout<<"Debe ser un valor mayor a cero. Reintente..." <<endl;
        cin >> masa ;
    }

    cout << "\nIngrese la altura inicial de su sistema, según sistema de referencia, en [m]: " << endl;
    cin >> altura_inicial;
    cout << "\nLa Energía Potencial inicial del sistema corresponde a " << energiaPotencial_inicial_1(masa, altura_inicial)<< " [Julios]." << endl;

    cout << "\nIngrese la altura final del objeto puntual, según sistema de referencia, en [m]: " << endl;
    cin >> altura_final;
    cout << "\nLa Energía Potencial final del sistema corresponde a " << energiaPotencial_final_1(masa, altura_final) << " [Julios]." << endl;

    cout <<"\nEl diferencial de energías corrrespode a " << diferencialPotencial_1(potencialInicial_1, potencialFinal_1) << " [Julios]. " << endl;

    if (energiaPotencial_inicial_1(masa, altura_inicial) > energiaPotencial_final_1(masa, altura_final)){

        cout << "\nSu objeto se encuentra cayendo." << endl;

    } else if (energiaPotencial_inicial_1(masa, altura_inicial) < energiaPotencial_final_1(masa, altura_final)) {

        cout << "\nSu objeto se encuentra subiendo." << endl;

    } else {

        cout <<"\nSu objeto no tiene energía potencial, según el sístema de referencia." << endl;
    }
}

float resultadoDelta_1;

float deltaCinetica(float Elongitud_final, float Elongitud_incial)
{
    float resultadoDelta;
    resultadoDelta = Elongitud_final - Elongitud_incial;
    resultadoDelta_1 = resultadoDelta;
    return resultadoDelta;
}

float resultadoElasticaInicial_1;

float energiaCinetica_inicial(float Elongitud_inicial, float constante)
{
    float resultadoElasticaInicial;
    resultadoElasticaInicial = (0.5) * constante * Elongitud_inicial;
    resultadoElasticaInicial_1 = resultadoElasticaInicial;
    return resultadoElasticaInicial;
}

float resultadoElasticaFinal_1;

float energiaCinetica_final(float Elongitud_final, float constante)
{
    float resultadoElasticaFinal;
    resultadoElasticaFinal = (0.5) * constante * Elongitud_final;
    resultadoElasticaFinal_1 = resultadoElasticaFinal;
    return resultadoElasticaFinal;
}

float diferencialElastico_1(float resultadoElasticaInicial_1, float resultadoElasticaFinal_1)
{
    float diferencialElastico;
    diferencialElastico = (resultadoElasticaFinal_1 - resultadoElasticaInicial_1);
    return diferencialElastico;  
}


void moduloElastico(){

    float Elongitud_inicial;
    float Elongitud_final;
    float constante;

    cout << "MÓDULO FÍSICO:\t\tCALCULADORA DE ENERGÍA POTENCIAL ELÁSTICA" << endl;
    cout << "\nPor favor, a continuación ingrese los datos solicitados:" << endl;

    cout << "\nIngrese la constante de elasticidad: " << endl;
    cin >> constante;
    while (constante == 0)
    {
        cout<<"Debe ser un valor diferente de cero. Reintente..." <<endl;
        cin >> constante;
    }

    cout << "\nIngrese la longitud inicial del resorte (si el resorte no se comprimido o estirado, ingrese 0):  " << endl;
    cin >> Elongitud_inicial;

    cout << "La Energía Potencial Elástica incial del sistema corresponde a " << energiaCinetica_inicial(Elongitud_inicial, constante) << " [Julios]" << endl;

    cout << "\nIngrese la longitud final del resorte: " << endl;
    cin >> Elongitud_final;

    cout << "La Energía Potencial Elástica final del sistema corresponde a " << energiaCinetica_final(Elongitud_final, constante) << " [Julios]" << endl;

    cout <<"\nEl diferencial de energías corrrespode a " << diferencialElastico_1(resultadoElasticaInicial_1, resultadoElasticaFinal_1) << " [Julios]. " << endl;

    if (energiaCinetica_inicial(Elongitud_inicial, constante) > energiaCinetica_final(Elongitud_final, constante)){

        cout << "\nEl resorte se está comprimiendo." << endl;

    } else if (energiaCinetica_inicial(Elongitud_inicial, constante) < energiaCinetica_final(Elongitud_final, constante)){

        cout << "\nEl resorte se está elongando." << endl;

    } else {

        cout <<"\nSu objeto no tiene energía potencial elástica, según el sístema de referencia." << endl;
    }
}

int main(){
    int eleccion;
    cout << "\t\t¡BIENVENIDO A LA CALCULADORA DE MÓDULO MATEMÁTICO Y FÍSICO!" << endl;
    cout << "\nPARA PODER INGRESAR, DEBE SELECCIONAR UNO DE LOS MÓDULOS: " << endl;
    cout << "\t1. MÓDULO MATEMÁTICO" << endl;
    cout << "\t2. MÓDULO FÍSICO" << endl;
    cout << "\t0. SALIR DEL PROGRAMA" << endl;

    cout <<"\nPor favor, ingrese a continuación a qué módulo desea ingresar: ";
    cin >> eleccion;

    switch (eleccion){
        case 0:
        cout << "¡HASTA PRONTO!" << endl;
        break;

        case 1:
        int eleccion_menu;
        cout << "\t\t¡BIENVENIDO AL MÓDULO MATEMÁTICO!\n\nPARA HACER CONVERSIONES, POR FAVOR SELECCIONA UNO DE LAS OPCIONES A CONTINUACIÓN:" << endl;
        cout <<"\t1. DIMENSIONES" << endl;
        cout <<"\t2. MASA" << endl;
        cout <<"\t3. MOVIMIENTO" << endl;
        cout <<"\t4. ELECTRICIDAD" << endl;
        cout <<"\t5. QUÍMICA" << endl;
        cout <<"\t6. ENERGÍA" << endl;
        cout <<"\t7. ALMACENAMIENTO DE DATOS" << endl;
        cout <<"\t8. TIEMPO" << endl; 
        cin >> eleccion_menu;

        switch(eleccion_menu){
            case 1:
                dimension();
            break;

            case 2:
                masa();
            break; 

            case 3:
                movimiento();
            break;

            case 4:
                electricidad();
            break;

            case 5:
                quimica();
            break;

            case 6:
                energia();
            break;

            case 7:
                bytes();
            break;
            
            case 8:
                tiempo();
            break;
            default:
            cout << "La opción ingresada no se encuentra en el menú." << endl;
break;
        }break;

        case 2:
        int eleccion_menu_1;
        cout << "\t\t¡BIENVENIDO AL MÓDULO FÍSICO!\n\nPARA UTILIZAR LA CALCULADORA, POR FAVOR SELECCIONA UNO DE LAS OPCIONES A CONTINUACIÓN:" << endl;
        cout <<"\t1. ENERGÍA CINÉTICA" << endl;
        cout <<"\t2. ENERGÍA POTENCIAL GRAVITACIONAL" << endl;
        cout <<"\t3. ENERGÍA POTENCIAL ELÁSTICA" << endl;
        cin >> eleccion_menu_1;
        switch(eleccion_menu_1){
            case 1:
                moduloCinetico();
            break;

            case 2:
                moduloPotencial();
            break; 

            case 3:
                moduloElastico();
            break;
        
        default:
            cout << "Su opción ingresada no se encunetra en el menú." << endl;
break;
    }
           
    return 0;
    }
}