TEL - 102 : "SEMINARIO DE PROGRAMACIÓN"


FECHA DE ENTREGA: 11/10/2022

GRUPO NÚMERO 4, PARALELO 201

INTEGRANTES:    MARTINA MADRID     ROL: 202230533 - 9
                JAZMÍN MONDACA     ROL: 202230553 - 3
                EDUARDO PALMA      ROL: 202130512 - 2

PROYECTO : "TRANSFORMADOR DE UNIDADES MATEMÁTICAS Y CALCULADORA DE ENERGÍA FÍSICA"


FECHA DE ENTREGA : SÁBADO 10 DE DICIEMBRE DE 2022, 23:59:59 HRS. (UTC - 3) HORA CONTINENTAL DE CHILE.

I. INTRODUCCIÓN

    Como equipo de trabajo planeamos desarrollar un programa de conversión de unidades, para poder solucionar problemas de física y matemática. Para solucionar estos problemas, hemos creado un "TRANSFORMADOR DE UNIDADES MATEMÁTICAS Y CALCULADORA DE ENERGÍA FÍSICA" para poder facilitar
    el cálculo de distintas unidades de medidas, para así verificar datos, entregar una inrterfaz gráfica y a su vez facilitar el desarrollo de los problemas. Para esto, hemos utilizado el programa Visual Studio Code, para poder crear el código base de cada módulo y sus respectivos menús,
    la terminal en LINUX, para poder corroborar que este compile y funcione correctamente, y por último y lo más importante, el uso del programa Qt, para poder crear la interfaz grñafica del programa, y la creación de los gráficos en los módulos, para que sean visualizados una vez se entreguen los datos solicitados, y seguido a esto su conversión. 

    
II. OBJETIVOS.

    - OBJETIVO GENERAL:    

    El objetivo general de nuestro proyecto contempla facilitar la transformación de distintos tipos de unidades de forma matemática y además complementarlo con gráficas físicas 
    para los distintos tipos de energía, los cuales son: Energía cinética, elástica o potencial gravitacional, por medio de la creación de un conversor de unidades y graficador físico en QT.

    Para el desarrollo de esto, se establecen los siguientes objetivos específicos:

    2.1) Desarrollar un menú principal donde indique para elegir si se utilizará el transformador matemático, o de unidades, o su contraparte física, de energías. Esto describirá un desglose 
    por dos ámbitos según su elección:

        2.2) Selección matemática - transformador de unidades:
               Realizará la implementación de un sub - menú que indique las principales conversiones de unidades de medida, solicitando un dato especifico inicial, para desglosar por pantalla las diversas 
               conversiones que posee según su unidad y magnitud inicial. 

                El sub - menú contendrá los siguientes índices, como consideración inicial, por tanto sujetos a modificaciones futuras: Dimensión (área, longitud y volúmen), masa, movimiento (aceleración y velocidad), 
                electricidad (carga eléctrica, intensidad eléctrica y potencial eléctrico), química (densidad y masa molar), energía (energía, potencia y temperatura), informática (almacenamiento de datos) y tiempo.
    
       2.3) Selección física - Calculadora de energía genérica:
               Presentará un sub - menú donde se indicará el tipo de energía que se desea calcular, los cuales son: Energía cinética, potencial gravitatoria, potencial y elástica. Para esto, solicitará los datos iniciales 
               y datos finales, para realizar el cálculo a través de las fórmulas físicas y graficarlos de forma comparativa en un gráfico de barras. El cuál se empezó a desarrollar en este hito 2.

    - OBJETIVO ESPECÍFICO:

    Para el desarrollo del objetivo especifico, en función de sus dependencias internas, se establecen las siguientes actividades a realizar para el desempeño correcto de la segunda entrega del proyecto (Hito 2), para el MÓDULO DE FÍSICA, 
    complementado de igual forma con un Task Tracker, anexado en "ISSUES" dentro de GitLab para su revisión.

    A la calendarización inicial de la entrega para el día VIERNES 09 de Diciembre, con su aplazamiento para el día SÁBADO 10 de Diciembre como fecha final de entrega, a las 23:59:59 horas, se desarrolla las siguientes actividades por los 
    estudiantes para trabajar de forma conjunta los diferentes ejes del código para el MÓDULO DE FÍSICA y MÓDULO DE MATEMÁTICA, dividiéndose las tareas de la siguiente forma:

    - MARTINA MADRID: Modificación, actualización de avances y mejoras de MÓDULO MATEMÁTICO Y MÓDULO FÍSICA.
    - JAZMÍN MONDACA: Modificación, actualización de avances y mejoras de MÓDULO MATEMÁTICO Y MÓDULO FÍSICA.
    - EDUARDO PALMA: Modificación, actualización de avances y mejoras de MÓDULO MATEMÁTICO Y MÓDULO FÍSICA.

    EL DIAGRAMA DE COMPONENTES con el cual explicaremos el funcionamiento de nuestras funciones en forma conjunta se encuentra disponible en el siguiente link: https://usmcl-my.sharepoint.com/:f:/g/personal/eduardo_palmao_usm_cl/EopCFmj1bClNm2DhTHlCkQMB5tim_NCDnuEAz7WiJBvDBg?e=JdAaK7

    Finalmente, el programa desarrollado funcionará con la finalidad de facilitar las transformaciones de unidades principales, los módulos físicos comparativos principales con sus gráficas en Qt, el cuál es Útil en las primeras tres físicas 
    (FIS100, FIS110 y FIS120), como en otros ramos (QUI010) y el Móduilo de matemática ya listo.

III. DESARROLLO

    EL repositorio se constituye desde la creación de la función principal, correspondiente al menú inicial del programa, donde le pregunta al usuario a qué sub - menú desea ingresar, pudiendo ser el matemático o físico.

    Una vez que el usuario ingresa en alguno de los dos sub - menús, en el lado matemático podrá ingresar un valor asociado a la transformación que desee realizar. EL programa le solicitará la MAGNITUD del dato que desea transformar y le mostrará 
    por pantalla todas las conversiones que desea realizar. Esperamos poder implementar en futuras entregas que, además de ingresar la magnitud, pueda ingresar las diversas unidades existentes y que también le arroje diferentes conversiones y no sea para una unidad estática. 
    Esto se repite para todas las elecciones en el menú matemático.

    En el caso físico, el usuario al ingresar en el sub - menú físico podrá seleccionar que tipo de energía desea comparar. Una vez dentro de esto, el programa le solicitará los datos iniciales y finales para realizar una comparativa del cambio de energía y, con ello, 
    generar gráficos asociados a los valores solicitados.

    El usuario podrá salir de los menús en cualquier momento que lo estime pertinente.

    De esta forma se construye y ejecuta el entregable construido a partir de diversas funciones para cada uno de los sub - menús deseados. En este tercer hito, hemos dado por finalizado el código de fuente, .

    Podemos dar por finalizada la ejecución de ambos módulos. Ambos funcionan y grafican correctamente en Qt de acuerdo a lo establecido en el proyecto.
    
IV. REQUISITOS DE COMPILACIÓN

    Los requisitos de compilación son los comunes para la ejecución del cogido para un archivo binario:

        g++ -o BINARIO NOMBRE_ARCHIVO.cpp

    Por ejemplo, se podría utilizar en este caso:

        g++ -o Tranformador_Unidades codigofinal.cpp

       Correspondiendo así al código completo localizado en GitLab para el proyecto.

V. INSTALACIÓN

    Como compilar y ejecutar el programa creado para los módulos de matemática y física (Ambos funcionan de igual manera, solamente que entregan y solicitan distintos datos):

	1. Descargar el repositorio. 

	2. Descargar el programa Qt (en caso de no tenerlo). 

	3. Dirigirse al directorio donde se encuentre el codigo.

	4. Seguido a esto, abrir Qt y ejecutar el código para que se vizualice.

	5. Seguir las indicaciones del programa, las cuales son: 

		5.1) Seleccionar el MÓDULO a utilizarse (matemática o física).

		5.2) Seleccionar el MENÚ que se presenta, de acuerdo al módulo elegido.

		5.3) Digitar los datos solicitados en la pantalla, para realizar la transformación correspondiente.

		5.4) Ya puede visualizar en pantalla la transformación correctamente.

		5.5) Para poder cambiar de módulo, debe de cerrar la ventana que se abre al ejecutar el código para empezar nuevamente.

		5.6) Una vez ya realizadas todas las transformaciones que necesite, cierre Qt para darle fin al uso del programa.


VI. TRACKLIST

    El TrackList utilizado se encuentra en GitLab, con sus retroalimentaciones correspondientes.

VII. REFERENCIAS

    Brian Kernighan, Dennis Ritchie. "The C programming lenguage" (1978). Prentice Hall, ISBN 9780131101630.

    Canal Programación ATS (Recuperado el 9 de octubre de 2022), Lista de reproducción Programación en C++ || Programación ATS. YOutube (https://www.youtube.com/watch?v=dJzLmjSJc2c&list=PLWtYZ2ejMVJlUu1rEHLC0i_oibctkl0Vh)


