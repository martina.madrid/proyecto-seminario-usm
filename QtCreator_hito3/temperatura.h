#ifndef TEMPERATURA_H
#define TEMPERATURA_H

#include <QDialog>

namespace Ui {
class temperatura;
}

class temperatura : public QDialog
{
    Q_OBJECT

public:
    explicit temperatura(QWidget *parent = nullptr);
    ~temperatura();

private slots:
    void on_calcularMate_clicked();

private:
    Ui::temperatura *ui;
};

#endif // TEMPERATURA_H
