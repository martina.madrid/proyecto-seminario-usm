#ifndef LONGITUD_H
#define LONGITUD_H

#include <QDialog>

namespace Ui {
class Longitud;
}

class Longitud : public QDialog
{
    Q_OBJECT

public:
    explicit Longitud(QWidget *parent = nullptr);
    ~Longitud();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Longitud *ui;
};

#endif // LONGITUD_H
