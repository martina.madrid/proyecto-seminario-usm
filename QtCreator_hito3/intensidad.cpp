#include "intensidad.h"
#include "ui_intensidad.h"
#include <math.h>
intensidad::intensidad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::intensidad)
{
    ui->setupUi(this);
}

intensidad::~intensidad()
{
    delete ui;
}

void intensidad::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float g = usuario3.toFloat();

        float mili_a = g* 1000;
        float kilo_a = g* pow(10,-3);
        float mega_a = g* pow(10,-6);
        float giga_a = g* pow(10,-9);
        float ab_a = g* 0.1;




        ui->lcdnumber_2->display(mili_a);
        ui->lcdnumber_3->display(kilo_a);
        ui->lcdnumber_4->display(mega_a);
        ui->lcdnumber_5->display(giga_a);
        ui->lcdnumber_6->display(ab_a);
}

