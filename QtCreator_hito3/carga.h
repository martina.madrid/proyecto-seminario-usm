#ifndef CARGA_H
#define CARGA_H

#include <QDialog>

namespace Ui {
class carga;
}

class carga : public QDialog
{
    Q_OBJECT

public:
    explicit carga(QWidget *parent = nullptr);
    ~carga();

private slots:
    void on_pushButton_clicked();

private:
    Ui::carga *ui;
};

#endif // CARGA_H
