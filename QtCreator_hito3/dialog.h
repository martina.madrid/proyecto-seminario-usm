#ifndef DIALOG_H
#define DIALOG_H
#include <dialog.h>
#include <cinetica.h>
#include <temperatura.h>
#include <area.h>
#include <electricidad.h>
#include <dimension.h>
#include <quimica.h>
#include <QMainWindow>
#include <QRectF>
#include <QRect>
#include <string>
#include <QtCharts>
#include <QChart>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QLegend>
#include <QBarCategoryAxis>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QWidget>
#include <QtGlobal>
#include <QDialog>
#include <movimientos.h>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    //void on_calcularMate_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
