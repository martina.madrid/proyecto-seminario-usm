#include "area.h"
#include "ui_area.h"
#include <math.h>
area::area(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::area)
{
    ui->setupUi(this);
}

area::~area()
{
    delete ui;
}



void area::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float area = usuario3.toFloat();

        float kilometros = area * (1 * pow(10, -6));

        float hectarea = area * 0.0001;

        float decimetros = area * (1 * pow(10, 3));

        float centimetros = area * (1 * pow(10, 4));

        float millas = area * (3.86 * pow(10,-7));


        float yardas = area * 1.196;

        float pies = area * 10.76;

        float pulgadas = area * 1550;




        ui->lcdnumber_2->display(kilometros);
        ui->lcdnumber_3->display(hectarea);
        ui->lcdnumber_4->display(decimetros);
        ui->lcdnumber_5->display(centimetros);
        ui->lcdnumber_6->display(millas);
        ui->lcdnumber_7->display(yardas);
        ui->lcdnumber_8->display(pies);
        ui->lcdnumber_9->display(pulgadas);



}

