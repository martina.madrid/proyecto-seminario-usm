#ifndef DIMENSION_H
#define DIMENSION_H
#include <area.h>
#include <QDialog>
#include <longitud.h>
#include <volumen.h>
namespace Ui {
class Dimension;
}

class Dimension : public QDialog
{
    Q_OBJECT

public:
    explicit Dimension(QWidget *parent = nullptr);
    ~Dimension();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Dimension *ui;
};

#endif // DIMENSION_H
