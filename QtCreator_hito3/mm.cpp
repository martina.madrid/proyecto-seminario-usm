#include "mm.h"
#include "ui_mm.h"

MM::MM(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MM)
{
    ui->setupUi(this);
}

MM::~MM()
{
    delete ui;
}

void MM::on_pushButton_clicked()
{
    QString martiloca = ui->dato2->text();

    float dato = martiloca.toFloat();
    float kilogramo_mol = dato * (1.01);

    ui->lcdnumber_2->display(kilogramo_mol);
}

