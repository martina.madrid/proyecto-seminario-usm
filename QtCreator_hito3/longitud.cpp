#include "longitud.h"
#include "ui_longitud.h"

Longitud::Longitud(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Longitud)
{
    ui->setupUi(this);
}

Longitud::~Longitud()
{
    delete ui;
}

void Longitud::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float lon = usuario3.toFloat();

        float kilometros = lon * 0.001;

        float hectarea = lon * 0.0001;

        float decimetros = lon * 10;

        float centimetros = lon * 100;

        float millas = lon * 0.0006;


        float yardas = lon * 1.093;

        float pies = lon * 3.28;

        float pulgadas = lon * 39.37;




        ui->lcdnumber_2->display(kilometros);
        ui->lcdnumber_3->display(hectarea);
        ui->lcdnumber_4->display(decimetros);
        ui->lcdnumber_5->display(centimetros);
        ui->lcdnumber_6->display(millas);
        ui->lcdnumber_7->display(yardas);
        ui->lcdnumber_8->display(pies);
        ui->lcdnumber_9->display(pulgadas);
}

