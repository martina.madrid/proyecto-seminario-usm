#ifndef DENSIDAD_H
#define DENSIDAD_H

#include <QDialog>

namespace Ui {
class densidad;
}

class densidad : public QDialog
{
    Q_OBJECT

public:
    explicit densidad(QWidget *parent = nullptr);
    ~densidad();

private slots:
    void on_pushButton_clicked();

private:
    Ui::densidad *ui;
};

#endif // DENSIDAD_H
