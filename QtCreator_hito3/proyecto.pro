#-------------------------------------------------
#
# Project created by QtCreator 2021-12-04T19:18:12
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = proyecto
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        aceleracion.cpp \
        area.cpp \
        carga.cpp \
        cinetica.cpp \
        densidad.cpp \
        dialog.cpp \
        dimension.cpp \
        electricidad.cpp \
        intensidad.cpp \
        longitud.cpp \
        main.cpp \
        bloque.cpp \
        mm.cpp \
        movimientos.cpp \
        quimica.cpp \
        temperatura.cpp \
        velocidad.cpp \
        volumen.cpp

HEADERS += \
        aceleracion.h \
        area.h \
        bloque.h \
        bloque.hpp \
        carga.h \
        cinetica.h \
        densidad.h \
        dialog.h \
        dimension.h \
        electricidad.h \
        intensidad.h \
        longitud.h \
        mm.h \
        movimientos.h \
        quimica.h \
        temperatura.h \
        velocidad.h \
        volumen.h

FORMS += \
        aceleracion.ui \
        area.ui \
        bloque.ui \
        carga.ui \
        cinetica.ui \
        densidad.ui \
        dialog.ui \
        dimension.ui \
        electricidad.ui \
        intensidad.ui \
        longitud.ui \
        mm.ui \
        movimientos.ui \
        quimica.ui \
        temperatura.ui \
        velocidad.ui \
        volumen.ui

