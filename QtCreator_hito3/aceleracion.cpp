#include "aceleracion.h"
#include "ui_aceleracion.h"

aceleracion::aceleracion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aceleracion)
{
    ui->setupUi(this);
}

aceleracion::~aceleracion()
{
    delete ui;
}

void aceleracion::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float e = usuario3.toFloat();

        float km_h2 = e* 12960;
        float km_m2 = e* 3.6;
        float milla_h2 = e* 8053;
        float pie_s2 = e* 3.2808;
        float gals = e* 100;




        ui->lcdnumber_2->display(km_h2);
        ui->lcdnumber_3->display(km_m2);
        ui->lcdnumber_4->display(milla_h2);
        ui->lcdnumber_5->display(pie_s2);
        ui->lcdnumber_6->display(gals);
}

