#ifndef ELECTRICIDAD_H
#define ELECTRICIDAD_H

#include <QDialog>
#include <carga.h>
#include <intensidad.h>
namespace Ui {
class electricidad;
}

class electricidad : public QDialog
{
    Q_OBJECT

public:
    explicit electricidad(QWidget *parent = nullptr);
    ~electricidad();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::electricidad *ui;
};

#endif // ELECTRICIDAD_H
