#ifndef VELOCIDAD_H
#define VELOCIDAD_H

#include <QDialog>

namespace Ui {
class velocidad;
}

class velocidad : public QDialog
{
    Q_OBJECT

public:
    explicit velocidad(QWidget *parent = nullptr);
    ~velocidad();

private slots:
    void on_pushButton_clicked();

private:
    Ui::velocidad *ui;
};

#endif // VELOCIDAD_H
