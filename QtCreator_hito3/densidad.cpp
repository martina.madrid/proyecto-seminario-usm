#include "densidad.h"
#include "ui_densidad.h"
#include <math.h>
densidad::densidad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::densidad)
{
    ui->setupUi(this);
}

densidad::~densidad()
{
    delete ui;
}

void densidad::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float i = usuario3.toFloat();

        float gramo_centimetro = i / 1;
        float gramo_metro = i / 1;



        ui->lcdnumber_2->display(gramo_centimetro);
        ui->lcdnumber_3->display(gramo_metro);

}

