#include "carga.h"
#include "ui_carga.h"
#include <math.h>
carga::carga(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::carga)
{
    ui->setupUi(this);
}

carga::~carga()
{
    delete ui;
}

void carga::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float i = usuario3.toFloat();

        float kilo_o = i* pow(10,-3);
        float mega_o = i* pow(10,-6);
        float giga_o = i* pow(10,-9);
        float ab_o = i* 1000000000;


        ui->lcdnumber_2->display(kilo_o);
        ui->lcdnumber_3->display(mega_o);
        ui->lcdnumber_4->display(giga_o);
        ui->lcdnumber_5->display(ab_o);
}

