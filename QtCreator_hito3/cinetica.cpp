#include "cinetica.h"
#include "ui_cinetica.h"

float ener1;
float *ener=NULL;

float ener2;
float *ener3=NULL;

cinetica::cinetica(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cinetica)
{
    ui->setupUi(this);
}

cinetica::~cinetica()
{
    delete ui;
}

void cinetica::on_CalcularFisica_clicked()
{



        QString usuario = ui->dato2->text();

        float masa = usuario.toFloat();

        QString usuario3 = ui->dato2_2->text();

        float Vincial = usuario3.toFloat();

        QString martilocaa = ui->dato2_3->text();

        float Vfinal = martilocaa.toFloat();

        float energiaCinetica_Final = ( (0.5) * masa ) * (Vfinal * Vfinal);

        float energiaCinetica_inicial;
        energiaCinetica_inicial = ( (0.5) * masa ) * (Vincial * Vincial);

        ui->lcdnumber_2->display(energiaCinetica_inicial);
        ui->lcdnumber_3->display(energiaCinetica_Final);


        //-----------------------------------------------//

            ener=new float;
            *ener=energiaCinetica_inicial;
            ener1=*ener;

            ener3 = new float;
            *ener3 = energiaCinetica_Final;
            ener2 = *ener3;



            QBarSet *set0 = new QBarSet("Energia cinetica inicial");
            QBarSet *set1 = new QBarSet("Energia cinetica final");

            *set0 << ener1;
            *set1 << ener2;
            // Crear series
            QBarSeries *series = new QBarSeries;
            series->append(set0);
            series->append(set1);

            series->setLabelsVisible(true);
            // Crear gráfico e insertar serie
                QChart *chart = new QChart();
                chart->setTitle("Energia cinetica inicial");
                chart->legend()->setVisible(true);
                chart->legend()->setAlignment(Qt::AlignBottom);
                chart->addSeries(series);
                chart->setAnimationOptions(QChart::SeriesAnimations);
                chart->createDefaultAxes();

                //Crear widget QChartView (e insertarlo en una ventana):
                ui->graphicsView->setChart(chart);
                ui->graphicsView->show();

}

