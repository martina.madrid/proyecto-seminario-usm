#include "velocidad.h"
#include "ui_velocidad.h"

velocidad::velocidad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::velocidad)
{
    ui->setupUi(this);
}

velocidad::~velocidad()
{
    delete ui;
}

void velocidad::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float d = usuario3.toFloat();

        float m_s = d* 1000;
        float km_h = d* 3600;
        float milla_s = d* 0.62;
        float milla_h = d* 2237;
        float pie_s = d* 3280;
        float velocidad_luz = d* 0.000003;
        float velocidad_sonido = d* 2.91;


        ui->lcdnumber_2->display(m_s);
        ui->lcdnumber_3->display(km_h);
        ui->lcdnumber_4->display(milla_s);
        ui->lcdnumber_5->display(milla_h);
        ui->lcdnumber_6->display(pie_s);
        ui->lcdnumber_7->display(velocidad_luz);
        ui->lcdnumber_8->display(velocidad_sonido);

}

