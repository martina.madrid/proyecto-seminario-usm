#ifndef ACELERACION_H
#define ACELERACION_H

#include <QDialog>

namespace Ui {
class aceleracion;
}

class aceleracion : public QDialog
{
    Q_OBJECT

public:
    explicit aceleracion(QWidget *parent = nullptr);
    ~aceleracion();

private slots:
    void on_pushButton_clicked();

private:
    Ui::aceleracion *ui;
};

#endif // ACELERACION_H
