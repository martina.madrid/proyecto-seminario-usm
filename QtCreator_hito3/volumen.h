#ifndef VOLUMEN_H
#define VOLUMEN_H

#include <QDialog>

namespace Ui {
class volumen;
}

class volumen : public QDialog
{
    Q_OBJECT

public:
    explicit volumen(QWidget *parent = nullptr);
    ~volumen();

private slots:
    void on_pushButton_clicked();

private:
    Ui::volumen *ui;
};

#endif // VOLUMEN_H
