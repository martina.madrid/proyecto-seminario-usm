#ifndef INTENSIDAD_H
#define INTENSIDAD_H

#include <QDialog>

namespace Ui {
class intensidad;
}

class intensidad : public QDialog
{
    Q_OBJECT

public:
    explicit intensidad(QWidget *parent = nullptr);
    ~intensidad();

private slots:
    void on_pushButton_clicked();

private:
    Ui::intensidad *ui;
};

#endif // INTENSIDAD_H
