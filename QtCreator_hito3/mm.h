#ifndef MM_H
#define MM_H

#include <QDialog>

namespace Ui {
class MM;
}

class MM : public QDialog
{
    Q_OBJECT

public:
    explicit MM(QWidget *parent = nullptr);
    ~MM();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MM *ui;
};

#endif // MM_H
