#ifndef MOVIMIENTOS_H
#define MOVIMIENTOS_H

#include <QDialog>
#include <velocidad.h>
#include <aceleracion.h>
namespace Ui {
class movimientos;
}

class movimientos : public QDialog
{
    Q_OBJECT

public:
    explicit movimientos(QWidget *parent = nullptr);
    ~movimientos();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::movimientos *ui;
};

#endif // MOVIMIENTOS_H
