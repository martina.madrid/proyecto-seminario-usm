#include "volumen.h"
#include "ui_volumen.h"
#include <math.h>
volumen::volumen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::volumen)
{
    ui->setupUi(this);
}

volumen::~volumen()
{
    delete ui;
}

void volumen::on_pushButton_clicked()
{
    QString usuario3 = ui->dato2->text();

        float vol = usuario3.toFloat();

        float kilometros = vol * (1* pow(10,-9));

        float onza = vol * 33814.02;

        float centimetros = vol * 1000000;

        float yardas = vol * 1.308;

        float pies = vol * 35.31;

        float pulgadas = vol * 61023;

        float galones = vol * 227.02;




        ui->lcdnumber_2->display(kilometros);
        ui->lcdnumber_3->display(onza);
        ui->lcdnumber_4->display(centimetros);
        ui->lcdnumber_5->display(yardas);
        ui->lcdnumber_6->display(pies);
        ui->lcdnumber_7->display(pulgadas);
        ui->lcdnumber_8->display(galones);

}

