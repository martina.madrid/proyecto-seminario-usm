#ifndef QUIMICA_H
#define QUIMICA_H

#include <QDialog>
#include <densidad.h>
#include <mm.h>
namespace Ui {
class quimica;
}

class quimica : public QDialog
{
    Q_OBJECT

public:
    explicit quimica(QWidget *parent = nullptr);
    ~quimica();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::quimica *ui;
};

#endif // QUIMICA_H
