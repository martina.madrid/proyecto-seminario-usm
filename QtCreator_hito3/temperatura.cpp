#include "temperatura.h"
#include "ui_temperatura.h"

temperatura::temperatura(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::temperatura)
{
    ui->setupUi(this);
}

temperatura::~temperatura()
{
    delete ui;
}


void temperatura::on_calcularMate_clicked()
{
    QString usuario2 = ui->dato2->text();

        float celcius = usuario2.toFloat();

        float kelvin = +273 + celcius;

        float faren = celcius * 1.8 + 32;

        float reaumur = celcius/1.25;

        float rankine = celcius*1.8 +491.67;


        ui->lcdnumber_2->display(celcius);
        ui->lcdnumber_3->display(kelvin);
        ui->lcdnumber_4->display(faren);
        ui->lcdnumber_5->display(reaumur);
        ui->lcdnumber_6->display(rankine);


}

