#include <iostream>
#include <math.h>


using namespace std;

float energiaCinetica_inicial1;

float energiaCinetica_inicial_1(float masa, float velocidad_inicial){

    float energiaCinetica_inicial;
    energiaCinetica_inicial = ( (0.5) * masa ) * (velocidad_inicial * velocidad_inicial);
    energiaCinetica_inicial1 = energiaCinetica_inicial;
    return energiaCinetica_inicial;
}

float energiaCinetica_final1;

float energiaCinetica_final_1(float masa, float velocidad_final){
    float energiaCinetica_final;
    energiaCinetica_final = ( (0.5) * masa ) * (velocidad_final * velocidad_final);
    energiaCinetica_final1 = energiaCinetica_final;
    return energiaCinetica_final;
}


float diferencialCineticas_1(float energiaInicial, float energiaFinal){
    float diferencialCinetica;
    diferencialCinetica = energiaFinal - energiaInicial;
    return diferencialCinetica;  
}

int main(){

    float masa;
    float velocidad_inicial;
    float velocidad_final;

/*  int Respuesta;

    bool isFound = true; */

    cout << "MÓDULO FÍSICO:\t\tCALCULADORA DE ENERGÍA CINÉTICA" << endl;
    cout << "\nPor favor, a continuación ingrese los datos solicitados:" << endl;

    cout << "\nMasa del objeto puntual en [Kg]:" << endl;
    cin >> masa;

    while (masa <= 0)
    {
        cout<<"Debe ser un valor mayor a cero. Reintente..." <<endl;
        cin >> masa ;
    }

    cout << "\nVelocidad incial del objeto puntual en [m/s²]: " << endl;
    cin >> velocidad_inicial;

    while (velocidad_inicial == 0)
    {
        cout<<"Debe ser un valor distinto a cero. Reintente..." <<endl;
        cin >> velocidad_inicial ;
    }

    cout << "\nLa Energía Cinética inicial del sistema corresponde a = " << energiaCinetica_inicial_1(masa, velocidad_inicial)<< " [Julios]." << endl;
    cout << "\nVelocidad final del objeto puntual en [m/s²]: " << endl;
    cin >> velocidad_final;

    cout << "\nLa Energía Cinética final del sistema corresponde a = " << energiaCinetica_final_1(masa, velocidad_final) << " [Julios]." << endl;
    cout <<"\nAhora, el diferencial de energías corrrespode a " << diferencialCineticas_1(energiaCinetica_inicial1, energiaCinetica_final1) << " [Julios]. " << endl;

    if (energiaCinetica_inicial_1(masa, velocidad_inicial) > energiaCinetica_final_1(masa, velocidad_final)){

        cout << "\nSu objeto se encuentra desacelerando." << endl;

    } else if (energiaCinetica_inicial_1(masa, velocidad_inicial) < energiaCinetica_final_1(masa, velocidad_final)) {

        cout << "\nSu objeto se encuentra acelerando." << endl;

    } else {

        cout <<"\nSu objeto se encuentra a una velocidad constante." << endl;
    }

    cout << "\n¿Qué desea realizar ahora?"<< endl;
    cout <<"1. Ingresar nuevos datos." << endl;
    cout <<"2. Volver atrás." << endl;

/*    cin >> Respuesta;

    switch (Respuesta){
        case 1:
        cout << "Re - iniciando el programa" << endl;
        cout << "--------------------------------------------------" << endl;
        
        isFound = false;

        break;
        
        case 2:
        cout << "Volviendo al menú anterior..." << endl;
        cout << "--------------------------------------------------" << endl;
break;
        default:
        cout << "Respuesta no válida. Reintente." << endl;
    }*/

return 0;
}