#include <iostream>
#include <math.h>


using namespace std;

float potencialInicial_1;

float energiaPotencial_inicial_1(float masa, float altura_inicial){

    float energiaPotencial_inicial;
    energiaPotencial_inicial = masa * 9.8 * altura_inicial;
    potencialInicial_1= energiaPotencial_inicial;
    return energiaPotencial_inicial;
}

float potencialFinal_1;

float energiaPotencial_final_1(float masa, float altura_final){

    float energiaPotencial_final;
    energiaPotencial_final = masa * 9.8 * altura_final;
    potencialFinal_1 = energiaPotencial_final;
    return energiaPotencial_final;
}


float diferencialPotencial_1(float potencialFinal_1, float potencialInicial_1){

    float diferencialPotencial;
    diferencialPotencial = (potencialFinal_1 - potencialInicial_1);
    return diferencialPotencial;  
}

int main(){

    float masa;
    float altura_inicial;
    float altura_final;

/*  int Respuesta;

    bool isFound = true; */

    cout << "MÓDULO FÍSICO:\t\tCALCULADORA DE ENERGÍA POTENCIAL" << endl;
    cout << "\nPor favor, a continuación ingrese los datos solicitados:" << endl;

    cout << "\nMasa del objeto puntual en [Kg]:" << endl;
    cin >> masa;

    while (masa <= 0)
    {
        cout<<"Debe ser un valor mayor a cero. Reintente..." <<endl;
        cin >> masa ;
    }

    cout << "\nIngrese la altura inicial de su sistema, según sistema de referencia, en [m]: " << endl;
    cin >> altura_inicial;
    cout << "\nLa Energía Potencial inicial del sistema corresponde a " << energiaPotencial_inicial_1(masa, altura_inicial)<< " [Julios]." << endl;

    cout << "\nIngrese la altura final del objeto puntual, según sistema de referencia, en [m]: " << endl;
    cin >> altura_final;
    cout << "\nLa Energía Potencial final del sistema corresponde a " << energiaPotencial_final_1(masa, altura_final) << " [Julios]." << endl;

    cout <<"\nEl diferencial de energías corrrespode a " << diferencialPotencial_1(potencialInicial_1, potencialFinal_1) << " [Julios]. " << endl;

    if (energiaPotencial_inicial_1(masa, altura_inicial) > energiaPotencial_final_1(masa, altura_final)){

        cout << "\nSu objeto se encuentra cayendo." << endl;

    } else if (energiaPotencial_inicial_1(masa, altura_inicial) < energiaPotencial_final_1(masa, altura_final)) {

        cout << "\nSu objeto se encuentra subiendo." << endl;

    } else {

        cout <<"\nSu objeto no tiene energía potencial, según el sístema de referencia." << endl;
    }

/*    cout << "\n¿Qué desea realizar ahora?"<< endl;
    cout <<"1. Ingresar nuevos datos." << endl;
    cout <<"2. Volver atrás." << endl; */

/*    cin >> Respuesta;

    switch (Respuesta){
        case 1:
        cout << "Re - iniciando el programa" << endl;
        cout << "--------------------------------------------------" << endl;
        
        isFound = false;

        break;
        
        case 2:
        cout << "Volviendo al menú anterior..." << endl;
        cout << "--------------------------------------------------" << endl;
break;
        default:
        cout << "Respuesta no válida. Reintente." << endl;
    }*/

return 0;
}
