#include <iostream>
#include <math.h>

using namespace std;

float resultadoDelta_1;

float deltaCinetica(float Elongitud_final, float Elongitud_incial)
{
    float resultadoDelta;
    resultadoDelta = Elongitud_final - Elongitud_incial;
    resultadoDelta_1 = resultadoDelta;
    return resultadoDelta;
}

float resultadoElasticaInicial_1;

float energiaCinetica_inicial(float Elongitud_inicial, float constante)
{
    float resultadoElasticaInicial;
    resultadoElasticaInicial = (0.5) * constante * Elongitud_inicial;
    resultadoElasticaInicial_1 = resultadoElasticaInicial;
    return resultadoElasticaInicial;
}

float resultadoElasticaFinal_1;

float energiaCinetica_final(float Elongitud_final, float constante)
{
    float resultadoElasticaFinal;
    resultadoElasticaFinal = (0.5) * constante * Elongitud_final;
    resultadoElasticaFinal_1 = resultadoElasticaFinal;
    return resultadoElasticaFinal;
}

float diferencialElastico_1(float resultadoElasticaInicial_1, float resultadoElasticaFinal_1)
{
    float diferencialElastico;
    diferencialElastico = (resultadoElasticaFinal_1 - resultadoElasticaInicial_1);
    return diferencialElastico;  
}


int main(){

    float Elongitud_inicial;
    float Elongitud_final;
    float constante;

/*  int Respuesta;

    bool isFound = true; */

    cout << "MÓDULO FÍSICO:\t\tCALCULADORA DE ENERGÍA POTENCIAL ELÁSTICA" << endl;
    cout << "\nPor favor, a continuación ingrese los datos solicitados:" << endl;

    cout << "\nIngrese la constante de elasticidad: " << endl;
    cin >> constante;
    while (constante == 0)
    {
        cout<<"Debe ser un valor diferente de cero. Reintente..." <<endl;
        cin >> constante;
    }

    cout << "\nIngrese la longitud inicial del resorte (si el resorte no se comprimido o estirado, ingrese 0):  " << endl;
    cin >> Elongitud_inicial;

    cout << "La Energía Potencial Elástica incial del sistema corresponde a " << energiaCinetica_inicial(Elongitud_inicial, constante) << " [Julios]" << endl;

    cout << "\nIngrese la longitud final del resorte: " << endl;
    cin >> Elongitud_final;

    cout << "La Energía Potencial Elástica final del sistema corresponde a " << energiaCinetica_final(Elongitud_final, constante) << " [Julios]" << endl;

    cout <<"\nEl diferencial de energías corrrespode a " << diferencialElastico_1(resultadoElasticaInicial_1, resultadoElasticaFinal_1) << " [Julios]. " << endl;

    if (energiaCinetica_inicial(Elongitud_inicial, constante) > energiaCinetica_final(Elongitud_final, constante)){

        cout << "\nEl resorte se está comprimiendo." << endl;

    } else if (energiaCinetica_inicial(Elongitud_inicial, constante) < energiaCinetica_final(Elongitud_final, constante)){

        cout << "\nEl resorte se está elongando." << endl;

    } else {

        cout <<"\nSu objeto no tiene energía potencial elástica, según el sístema de referencia." << endl;
    }


    return 0;
}

/*    cout << "\n¿Qué desea realizar ahora?"<< endl;
    cout <<"1. Ingresar nuevos datos." << endl;
    cout <<"2. Volver atrás." << endl; */

/*    cin >> Respuesta;

    switch (Respuesta){
        case 1:
        cout << "Re - iniciando el programa" << endl;
        cout << "--------------------------------------------------" << endl;
        
        isFound = false;

        break;
        
        case 2:
        cout << "Volviendo al menú anterior..." << endl;
        cout << "--------------------------------------------------" << endl;
break;
        default:
        cout << "Respuesta no válida. Reintente." << endl;
    }*/