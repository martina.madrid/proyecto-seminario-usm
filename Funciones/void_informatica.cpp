#include <iostream>

using namespace std;

void byte0(float magnitud_informatica){
     float byte_kilobyte = magnitud_informatica * (9.77 * pow(10, -4));
     float byte_megabyte = magnitud_informatica * (9.54 * pow(10, -7));
     float byte_gigabyte = magnitud_informatica * (9.31 * pow(10, -10));
     float byte_terabyte = magnitud_informatica * (9.09 * pow(10, -13));
     float byte_petabyte = magnitud_informatica * (8.88 * pow(10, -16));
     float byte_exabyte = magnitud_informatica * (8.67 * pow(10, -19));

     cout << "\t\t[kiloBytes] =" << byte_kilobyte << endl;
     cout << "\t\t[MegaBytes] =" << byte_megabyte << endl;
     cout << "\t\t[GigaBytes] =" << byte_gigabyte << endl;
     cout << "\t\t[TeraBytes] =" << byte_terabyte << endl;
     cout << "\t\t[PetaBytes] =" << byte_petabyte << endl;
     cout << "\t\t[ExaBytes] =" << byte_exabyte << endl;

}

void glosario_informatica(){

    float numero_informatica;

    //ALMACENAMIENTO DE DATOS
    cout << "INGRESE LA MAGNITUD EN [Bytes]: ";
    cin >> numero_informatica;

    cout << "TUS DATOS INGRESADOS SON:" << numero_informatica << "[Bytes]" << endl;
    cout << "\n\n LAS TRANSFORMACIONES SON LAS SIGUIENTES: " << endl;
    byte0(numero_informatica);

}

int main(){

    glosario_informatica();

    return 1;

}