/* quimica:
Densidad:
Gramo por centìmetro cúbico (g/cm³) 10-3
Kilogramo por metro cúbico (kg/m³) 1
Gramo por metro cúbico (g/m³) 1.000

Masa Molar:
Gramos por mol (g/mol) 1,01
Kilogramos por mol (kg/mol) 1,01×10-3 */

#include <iostream>
#include <cmath>
using namespace std;

int main(void){

    int opcion1, opcion2;
    float gramo_centimetro, gramo_metro, gramo_mol, kilogramo_metro, kilogramo_mol;

    cout << "INGRESE LA OPCION QUE DESEA: " << endl;
    cout << "1. MEDIDAS DE DENSIDAD" << endl;
    cout << "2. MEDIDAS DE MASA MOLAR" << endl; 
    cin >> opcion1;

    switch (opcion1){
        
        case 1:
        
        cout << "---MEDIDAS DE DENSIDAD---" << endl;
        cout << "Ingrese la opcion que desea: " << endl;
        cout << "1. Gramo por centìmetro cúbico a Kilogramo por metro cúbico" << endl;
        cout << "2. Gramo por centìmetro cúbico a Kilogramo a Gramo por metro cúbico" << endl;
        cout << "3. Kilogramo por metro cubico a Gramo por centimetro cubico" << endl;
        cout << "4. Kilogramo por metro cubico a Gramo por metro cubico" << endl; 
        cin >> opcion2;

        switch (opcion2){

            case 1:
                cout << "---Gramo por centìmetro cúbico a Kilogramo por metro cúbico---" << endl;
                cout << "Ingrese la cantidad de gramos por centimetros cubicos a convertir: " << endl;
                cin >> gramo_centimetro;
                kilogramo_metro = gramo_centimetro * pow(10,-3);
                cout << gramo_centimetro << "Gramos por centimetros cubicos equivalen a " << kilogramo_metro << " kilogramos por metros cubicos." << endl;
                break;

            case 2:
                cout << "---Gramo por centìmetro cúbico a Kilogramo a Gramo por metro cúbico---" << endl;
                cout << "Ingrese la cantidad de gramos por centimetros cubicos a convertir: " << endl;
                cin >> gramo_centimetro;
                gramo_metro = gramo_centimetro * pow(10,-3); 
                cout << gramo_centimetro << "Gramos por centimetros cubicos equivalen a " << gramo_metro << " gramos por metros cubicos." << endl;
                break;

            case 3:
                cout << "---Kilogramo por metro cubico a Gramo por centimetro cubico---" << endl;
                cout << "Ingrese la cantidad de kilogramos por metro cubico a convertir: " << endl;
                cin >> kilogramo_metro;
                gramo_centimetro = kilogramo_metro / 1;
                cout << kilogramo_metro << "Kilogramos por metros cubicos equivalen a " << gramo_centimetro << " gramos por centimetros cubicos." << endl;
                break;

            case 4:
                cout << "---Kilogramo por metro cubico a Gramo por metro cubico---" << endl;
                cout << "Ingrese la cantidad de kilogramos por metro cubico a convertir: " << endl;
                cin >> kilogramo_metro;
                gramo_metro = kilogramo_metro / 1;
                cout << kilogramo_metro << "Kilogramos por metros cubicos equivalen a " << gramo_metro << " gramos por metros cubicos." << endl;
                break;

        }
        
        break; 

    case 2:

        cout << "---MEDIDAS DE MASA MOLAR---" << endl;
        cout << "Ingrese la opcion que desea: " << endl;
        cout << "1. Gramos por mol a Kilogramos por mol" << endl;
        cout << "2. Kilogramos por mol a Gramos por mol" << endl;
        cin >> opcion2;

        switch (opcion2){

            case 1:
                cout << "---Gramos por mol a Kilogramos por mol---" << endl;
                cout << "Ingrese la cantidad de Gramos por moles a convertir: " << endl;
                cin >> gramo_mol;
                kilogramo_mol = gramo_mol * (1,01);
                cout << gramo_mol << "Gramos por mol equivalen a " << gramo_mol << " kilogramos por mol." << endl;
                break;

            case 2: 
                cout << "---Kilogramos por mol a Gramos por mol---" << endl;
                cout << "Ingrese la cantidad de Kilogramos por moles a convertir: " << endl;
                cin >> kilogramo_mol;
                gramo_mol = kilogramo_mol / ((1,01) * pow(10,-3));
                cout << kilogramo_mol << "Kilogramos por mol equivalen a " << kilogramo_mol << " gramos por mol." << endl;
                break;
        }
    

    default:

        cout << "La opcion ingresada no se encuentra en el menu." << endl;
        break;
    }
    
}
