#include <iostream>
#include<cmath>

using namespace std;

int main(void){
    
    int opcion1, opcion2;
    float kilometros, hectaria, metro, decimetros, centimetros, milimetros, micrometros, nanometros, milla, yarda, pie, pulgada, onzas, galones;

    
    cout<< "Ingrese la opción que desea: "<<endl;
    cout<< "1. Dimensiones."<<endl;
    cout<< "2. Movimiento."<<endl;
    cout<< "3. Electricidad."<<endl;
    cin>> opcion1;
    
    switch(opcion1){
        
        case 1:
        cout<< "-----Dimensiones-----"<<endl;
        cout<< "Ingrese la opción que desea: "<<endl;
        cout<< "1. Área."<<endl;
        cout<< "2. Longitud."<<endl;
        cout<< "3. Volumen."<<endl;
        cin>>opcion2;
        
        switch(opcion2){
            case 1:

                float a;
                cout<< "-----Área-----"<<endl;
                cout<< "Ingrese la opción que desea en [Metros cuadrados]: "<<endl;
                cin>>a;
                kilometros = a* 0.000001;
                hectaria = a* 0.0001;
                decimetros = a* 100;
                centimetros = a* 10000;
                milimetros = a* 1000000;
                micrometros = a* 1000000000000;
                nanometros = a* 1000000000000000000;
                milla = a* (3.86* pow(10,-7));
                yarda = a* 1196;
                pie = a* 10.76;
                pulgada = a* 1550;

                cout<<"Unidad en Kilometros al cuadrado --> "<<kilometros<<endl;
                cout<<"Unidad en Hectaría al cuadrado --> "<<hectaria<<endl;
                cout<<"Unidad en Decimetros al cuadrado --> "<<decimetros<<endl;
                cout<<"Unidad en Centímetros al cuadrado --> "<<centimetros<<endl;
                cout<<"Unidad en Milimetros al cuadrado --> "<<milimetros<<endl;
                cout<<"Unidad en Micrometros al cuadrado --> "<<micrometros<<endl;
                cout<<"Unidad en Milla al cuadrado --> "<<milla<<endl;
                cout<<"Unidad en Yarda al cuadrado --> "<<yarda<<endl;
                cout<<"Unidad en Pie al cuadrado --> "<<pie<<endl;
                cout<<"Unidad en Pulgada al cuadrado --> "<<pulgada<<endl;
    
            break;

            case 2:

                float b;
                cout<< "-----Longitud-----"<<endl;
                cout<< "Ingrese la opción que desea [Metros]: "<<endl;
                cin>>b;
                kilometros = b* 0.001;
                hectaria = b* 0.0001;
                decimetros = b* 10;
                centimetros= b* 100;
                milimetros = b* 1000;
                micrometros = b* 1000000;
                nanometros = b* 1000000000;
                milla = b* 0.0006;
                yarda = b* 1.093;
                pie = b* 3.28;
                pulgada = b* 39.37;

                cout<<"Unidad en Kilometros--> "<<kilometros<<endl;
                cout<<"Unidad en Hectaría--> "<<hectaria<<endl;
                cout<<"Unidad en Decimetros--> "<<decimetros<<endl;
                cout<<"Unidad en Centímetros--> "<<centimetros<<endl;
                cout<<"Unidad en Milimetros--> "<<milimetros<<endl;
                cout<<"Unidad en Micrometros--> "<<micrometros<<endl;
                cout<<"Unidad en Milla--> "<<milla<<endl;
                cout<<"Unidad en Yarda--> "<<yarda<<endl;
                cout<<"Unidad en Pie--> "<<pie<<endl;
                cout<<"Unidad en Pulgada--> "<<pulgada<<endl;
    
            break;

            case 3:

                float c;
                cout<< "-----Volumen-----"<<endl;
                cout<< "Ingrese la opción que desea en [Metros cúbicos]: "<<endl;
                cin>>c;
                kilometros = c* (1* pow(10,-9));
                onzas = c* 33814.02;
                decimetros = c* 1000;
                centimetros= c* 1000000;
                milimetros = c* 1000000000;
                yarda = c* 1.308;
                pie = c* 35.31;
                pulgada = c* 61023;
                galones = c* 227.02;

                cout<<"Unidad en Kilometros cúbicos--> "<<kilometros<<endl;
                cout<<"Unidad en Onzas--> "<<onzas<<endl;
                cout<<"Unidad en Decimetros cúbicos--> "<<decimetros<<endl;
                cout<<"Unidad en Centimetros cúbicos--> "<<centimetros<<endl;
                cout<<"Unidad en Milimetros cúbicos--> "<<milimetros<<endl;
                cout<<"Unidad en Yarda cúbica--> "<<yarda<<endl;
                cout<<"Unidad en Pie cúbicos--> "<<pie<<endl;
                cout<<"Unidad en Pulgada cúbicos--> "<<pulgada<<endl;
                cout<<"Unidad en Galones cúbicos--> "<<galones<<endl;

            break;

            default: 
                cout<< "La opción no es correcta."<<endl;
            break;
        }
        break;
        
        case 2:
        
            cout<< "-----Movimiento-----"<<endl;
            cout<< "Ingrese la opción que desea: "<<endl;
            cout<< "1. Velocidad."<<endl;
            cout<< "2. Aceleración."<<endl;
            cin>>opcion2;
                switch(opcion2){

                    case 1:
                    float d;
                    float m_s, km_h, mm_s, micro_s, milla_s, milla_h, pie_s, velocidad_luz, velocidad_sonido;
                    cout<<"-----Velocidad-----"<<endl;
                    cout<<"Ingrese la opción que desea en [km/s]: "<<endl;
                    cin>>d;
                    m_s = d* 1000;
                    km_h = d* 3600;
                    mm_s = d* 1000000;
                    micro_s = d* 1000000000;
                    milla_s = d* 0.62;
                    milla_h = d* 2237;
                    pie_s = d* 3280;
                    velocidad_luz = d* 0.000003;
                    velocidad_sonido = d* 2.91;

                    cout<<"Unidad en Metro por segundo--> "<<m_s<<endl;
                    cout<<"Unidad en Kilometro por hora--> "<<km_h<<endl;
                    cout<<"Unidad en milimetro por segundo--> "<<mm_s<<endl;
                    cout<<"Unidad en Micrometro por segundo--> "<<micro_s<<endl;
                    cout<<"Unidad en Milla por segundo--> "<<milla_s<<endl;
                    cout<<"Unidad en Milla por hora--> "<<milla_h<<endl;
                    cout<<"Unidad en Pies por segundo--> "<<pie_s<<endl;
                    cout<<"Unidad en Velocidad de la luz--> "<<pie_s<<endl;
                    cout<<"Unidad en Velocidad del sonido--> "<<pie_s<<endl;
                    break;

                    case 2:

                    float e;
                    float km_h2, km_m2, micro_s2, milla_s2, milla_h2, pie_s2, gals;
                    cout<<"-----Aceleración-----"<<endl;
                    cout<<"Ingrese la opción que desea en [m/s²]: "<<endl;
                    cin>>e;
                    km_h2 = e* 12960;
                    km_m2 = e* 3.6;
                    milla_h2 = e* 8053;
                    pie_s2 = e* 3.2808;
                    gals = e* 100;

                    cout<<"Unidad en Kilometro por hora--> "<<km_h2<<endl;
                    cout<<"Unidad en Kilometro por minuto--> "<<km_m2<<endl;
                    cout<<"Unidad en milla por hora--> "<<milla_h2<<endl;
                    cout<<"Unidad en Pie por segundo--> "<<pie_s2<<endl;
                    cout<<"Unidad en Gals--> "<<gals<<endl;


                }   
        break;

        default: 
            cout<< "La opción no es correcta."<<endl;
        break;

        case 3:

            cout<< "-----Electricidad-----"<<endl;
            cout<< "Ingrese la opción que desea: "<<endl;
            cout<< "1. Carga eléctrica."<<endl;
            cout<< "2. Intensidad elétrica."<<endl;
            cout<< "3. Potencial eléctrico."<<endl;
            cout<< "4. Resistencia eléctrica."<<endl;
            cin>>opcion2;
            switch(opcion2){

                case 1:
                float f;
                float nano_c, micro_c, mili_c, kilo_c, mega_c, mili_a, amperio_h, faraday, elemental;
                cout<<"-----Carga Eléctrica-----"<<endl;
                cout<<"Ingrese la opción que desea en Culombios [C]: "<<endl;
                cin>>f;
                nano_c = f* 1000000000;
                micro_c = f* 1000000;
                mili_c = f* 1000;
                kilo_c = f* pow(10,-3);
                mega_c = f* pow(10,-6);
                mili_a = f* 0.28;
                amperio_h = f* (2.78*pow(10,-4));
                faraday = f* (1.04*pow(10,-5));
                elemental = f* (6,24*pow(10,18));

                case 2:
                float g;
                float nano_a, micro_a, mili_a, kilo_a, mega_a, giga_a, ab_a;
                cout<<"-----Intensidad Eléctrica-----"<<endl;
                cout<<"Ingrese la opción que desea en Amperios [A]: "<<endl;
                cin>>g;
                nano_a = g* 1000000000;
                micro_a = g* 1000000;
                mili_a = g* 1000;
                kilo_a = g* pow(10,-3);
                mega_a = g* pow(10,-6);
                giga_a = g* pow(10,-9);
                ab_a = g* 0.1;

                cout<<"Unidad en Nanoamperios--> "<<nano_a<<endl;
                cout<<"Unidad en Microamperios--> "<<micro_a<<endl;
                cout<<"Unidad en Miliamperios--> "<<mili_a<<endl;
                cout<<"Unidad en Kiloamperios--> "<<kilo_a<<endl;
                cout<<"Unidad en Megamperios--> "<<mega_a<<endl;
                cout<<"Unidad en Gigamperios--> "<<giga_a<<endl;
                cout<<"Unidad en Abamperios--> "<<ab_a<<endl;

                case 3:
                float h;
                float nano_v, micro_v, mili_v, kilo_v, mega_v, giga_v, ab_v;
                cout<<"-----Potencial Eléctrico-----"<<endl;
                cout<<"Ingrese la opción que desea en Voltios [V]: "<<endl;
                cin>>h;
                nano_v = h* 1000000000;
                micro_v = h* 1000000;
                mili_v = h* 1000;
                kilo_v = h* pow(10,-3);
                mega_v = h* pow(10,-6);
                giga_v = h* pow(10,-9);
                ab_v = h* 100000000;

                cout<<"Unidad en Nanovoltios--> "<<nano_v<<endl;
                cout<<"Unidad en Microvoltios--> "<<micro_v<<endl;
                cout<<"Unidad en Milivoltios--> "<<mili_v<<endl;
                cout<<"Unidad en Kilovoltios--> "<<kilo_v<<endl;
                cout<<"Unidad en Megavoltios--> "<<mega_v<<endl;
                cout<<"Unidad en Gigavoltios--> "<<giga_v<<endl;
                cout<<"Unidad en Abvoltios--> "<<ab_v<<endl;

                case 4:
                float i;
                float nano_o, micro_o, mili_o, kilo_o, mega_o, giga_o, ab_o;
                cout<<"-----Resistencia Eléctrica-----"<<endl;
                cout<<"Ingrese la opción que desea en Ohmios: "<<endl;
                cin>>i;
                nano_o = i* 1000000000;
                micro_o = i* 1000000;
                mili_o = i* 1000;
                kilo_o = i* pow(10,-3);
                mega_o = i* pow(10,-6);
                giga_o = i* pow(10,-9);
                ab_o = i* 1000000000;

                cout<<"Unidad en Nanoohmios--> "<<nano_o<<endl;
                cout<<"Unidad en Microomhios--> "<<micro_o<<endl;
                cout<<"Unidad en Miliohmios--> "<<mili_o<<endl;
                cout<<"Unidad en Kiloomhios--> "<<kilo_o<<endl;
                cout<<"Unidad en Megaomhios--> "<<mega_o<<endl;
                cout<<"Unidad en Gigaomhios--> "<<giga_o<<endl;
                cout<<"Unidad en Abomhios--> "<<ab_o<<endl;

            }
        break;

        default: 
            cout<< "La opción no es correcta."<<endl;
        break;
        
    }
    
    return 0;
}
