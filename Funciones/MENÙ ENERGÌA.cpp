/* Formulas Energia 
Energìa:
Introduzca el número de Kilovatio (kWh) 3,77×10-7
Introduzca el número de Megajulio (MJ) 1,36×10-6
Introduzca el número de Kilojulio (kJ) 1,36×10-3
Introduzca el número de Julio (J) 1,36

Potencia:
Introduzca el número de Millivatio 1
Introduzca el número de Vatio (W) 10-3
Introduzca el número de Kilovatio (kW) 10-6
Introduzca el número de Julio por segundo (J/s) 10-3

Temperatura:
Kelvin (K) 255,93
Celsius (C) -17,22
Fahrenheit (F)
Reaumur (R) -13,78
Rankine 460,67 */


#include <iostream> 
#include <cmath>
using namespace std;

int main(void){

    int opcion1, opcion2;
    float kilovatio, megajulio, kilojulio, julio, millivatio, vatio, kilovatio, julio_psegundo, celsius, kelvin, fahrenheit, reaumur, rankine;

    cout << "INGRESE LA OPCION QUE DESEA: " << endl;
    cout << "1. MAGNITUD DE ENERGIA" << endl;
    cout << "2. MAGNITUD DE POTENCIA" << endl; 
    cout << "3. CONVERSION DE GRADOS (TEMPERATURA)" << endl;
    cin >> opcion1;

    switch (opcion1){
        
        case 1:
        
        cout << "---MAGNITUD DE ENERGIA [Kilovatio]---" << endl;
        cout << "Ingrese la opcion que desea: " << endl;
        cout << "1. Kilovatio a Megajulio" << endl;
        cout << "2. KIlovatio a Kilojulio" << endl;
        cout << "3. Kilovatio a Julio" << endl;
        cin >> opcion2;

        switch (opcion2){

            case 1:
                cout << "---Kilovatio a Megajulio--" << endl;
                cout << "Ingrese la cantidad de Kilovatios a convertir: " << endl;
                cin >> kilovatio;
                megajulio = kilovatio * ((3,77) * pow(10,-7));
                cout << kilovatio << "Kilovatios equivalen a " << megajulio << " Megajulios." << endl;
                break;

            case 2:
                cout << "---Kilovatio a Kilojulio---" << endl;
                cout << "Ingrese la cantidad de Kilovatios a convertir: " << endl;
                cin >> kilovatio;
                kilojulio = kilovatio * ((1,36) * (10,-6));
                cout << kilovatio << "Kilovaltios equivalen a " << kilojulio << " Kilojulios." << endl;
                break;

            case 3:
                cout << "---Kilovatio a Julio---" << endl;
                cout << "Ingrese la cantidad de Kilovatios a convertir: " << endl;
                cin >> kilovatio;
                julio = kilovatio * 1,36;
                cout << kilovatio << "Kilovaltios equivalen a " << julio << " Julios." << endl;
                break;

        }
        
        break; 

    case 2:

        cout << "---MAGNITUD DE POTENCIA [Millivatio]---" << endl;
        cout << "Ingrese la opcion que desea: " << endl;
        cout << "1. Millivatio a Vatio" << endl;
        cout << "2. Millivatio a Kilovatio" << endl;
        cout << "3. Millivatio a Julio por Segundo" << endl;
        cin >> opcion2;

        switch (opcion2){

            case 1:
                cout << "---Millivatio a Vatio--" << endl;
                cout << "Ingrese la cantidad de Millivatios a convertir: " << endl;
                cin >> millivatio;
                vatio = millivatio * pow(10,-3);
                cout << millivatio << "Millivatios equivalen a " << vatio << " Vatios." << endl;
                break;

            case 2:
                cout << "---Millivatio a Kilovatio---" << endl;
                cout << "Ingrese la cantidad de Millivatios a convertir: " << endl;
                cin >> millivatio;
                kilovatio = millivatio * pow(10,-6);
                cout << millivatio << "Millivatios equivalen a " << kilovatio << " Kilovatios." << endl;
                break;

            case 3:
                cout << "---Millivatio a Julio por Segundo---" << endl;
                cout << "Ingrese la cantidad de Millivatios a convertir: " << endl;
                cin >> millivatio;
                julio_psegundo = millivatio * pow(10,-3);
                cout << millivatio << "Millivatios equivalen a " << julio_psegundo << " Julios por segundo." << endl;
                break;

        }
        break; 

        case 3:

        cout << "---CONVERSION DE TEMPERATURA [CELSIUS]---" << endl;
        cout << "Ingrese la opcion que desea: " << endl;
        cout << "1. Celsius a Kelvin" << endl;
        cout << "2. Celsius a Fahrenheit" << endl;
        cout << "3. Celsius a Reaumur" << endl;
        cout << "4. Celsius a Rankine" << endl;
        cin >> opcion2;

        switch (opcion2){

            case 1:
                cout << "---Celsius a Kelvin--" << endl;
                cout << "Ingrese la cantidad de grados a convertir: " << endl;
                cin >> celsius;
                kelvin = celsius * 255,93; 
                cout << celsius << "Grados Celsius equivalen a " << kelvin << " grados Kelvin." << endl;
                break;

            case 2:
                cout << "---Celsius a Fahrenheit---" << endl;
                cout << "Ingrese la cantidad de grados a convertir: " << endl;
                cin >> celsius;
                fahrenheit = celsius * 1;
                cout << celsius << "Grados Celsius equivalen a " << fahrenheit << " grados Fahrenheit." << endl;
                break;

            case 3:
                cout << "---Celsius a Reaumur---" << endl;
                cout << "Ingrese la cantidad de grados a convertir: : " << endl;
                cin >> celsius;
                reaumur = celsius * (-13,78);
                cout << celsius << "Grados Celsius equivalen a " << reaumur << " grados reaumur." << endl;
                break;

            case 4:
                cout << "---Celsius a Rankine---" << endl;
                cout << "Ingrese la cantidad de grados a convertir: " << endl;
                cin >> celsius;
                rankine = celsius * (460,67);
                cout << celsius << "Grados Celsius equivalen a " << rankine << " grados Rankine." << endl;
                break;
    

    default:

        cout << "La opcion ingresada no se encuentra en el menu." << endl;
        break;
    }
    
}
